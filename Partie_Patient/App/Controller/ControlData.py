from Model.BDD import BDD

class ControlData:
    """
    Classe qui permet de gérer les données de l'application
    """
    def __init__(self, nom, prenom, mdp):
        """
        Constructeur de la classe
        """
        self.bdd = BDD()
        self.patient = self.bdd.selectPatientByNom(nom, prenom, mdp)[0]
        self.listProgram = self.bdd.selectAffectationByPatient(self.patient)

    def formatStr(self, chaine):
        """
        Méthode qui permet de mettre au bon format une chaîne de caractères pour l'insérer dans la base de données
        """
        if "'" in chaine :
            sous_chaine = chaine.split("'")
            res = ""
            for c in sous_chaine :
                if c == sous_chaine[-1] :
                    res += c
                else :
                    res += c +"\\'"
        else :
            res = chaine
        return res


    ######################################### PROGRAM REALISE #############################################

    def addProgramRealise(self, exercice, date, fichierJson):
        """
        Méthode qui permet d'ajouter un exercice réalisé
        """
        fichierJson = self.formatStr(fichierJson)
        self.bdd.insertRealisation(self.patient, exercice, date, fichierJson)
