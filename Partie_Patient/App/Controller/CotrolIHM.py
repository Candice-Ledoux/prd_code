import sys

import cv2
import numpy as np
from PyQt5.QtGui import QImage, QPixmap
from PyQt5 import QtGui

from Ressources.config import *
import Ressources.GlobalVar as GBV
from View.PopUpUI import *
from Model.Model import *
from Controller.ControlData import ControlData
from Controller.JsonParser import JsonParser
from View import MotionTrackingUI, MenuUI, WelcomeUI

import mediapipe as mp  # Import mediapipe
mp_drawing = mp.solutions.drawing_utils  # Drawing helpers
mp_pose = mp.solutions.pose  # Mediapipe Solutions


############################################## WELCOME : VUE 1 #########################################################
class Welcome(QtWidgets.QMainWindow, WelcomeUI.Ui_welcomeWindow):
    """
    Classe qui gère les méthodes de l'IHM WelcomeUI
    """
    def __init__(self, parent=None):
        """ Constructeur de la classe """
        super(Welcome, self).__init__(parent)
        self.setupUi(self)
        self.connectActions()
        self.show()

    def connectActions(self):
        """ Méthode qui définit les actions des boutons """
        self.startApp.mousePressEvent = self.launchApp

    def launchApp(self, event):
        """ Méthode qui permet de se connecter à l'application et qui accède à la vue MenuUI"""
        # Navigate to the Menu
        popUp = PopUpUI(self.startApp)
        if popUp.patient != []:
            menuPage = Menu(popUp.patient[0])
            self.setCentralWidget(menuPage)

    def closeEvent(self, event):
        """ Méthode qui ferme l'application et qui arrête la caméra si elle fonctionne """
        print("close")
        if GBV.isCapturing == True:
            print("fin enregistrement")
            GBV.isCapturing = False
            GBV.cap.release()
            GBV.timer.stop()
        event.accept()
        sys.exit(0)


################################################ MENU : VUE 2 ##########################################################
class Menu(QtWidgets.QMainWindow, MenuUI.Ui_menuWindow):
    """
    Classe qui gère les méthodes de l'IHM MenuUI
    """
    def __init__(self, pat, parent=None):
        """ Constructeur de la classe """
        super(Menu, self).__init__(parent)

        self.data = ControlData(pat.nom, pat.prenom, pat.mdp)

        # Path to find the models (file list)
        self.selectedFileName = ""
        self.selectedFileExtension = ""

        self.setupUi(self)
        self.connectActions()
        self.initList()

    def connectActions(self):
        """ Méthode qui définit les actions des boutons """
        self.selectSeqButton.clicked.connect(self.selectSeq)
        self.upload_btn.clicked.connect(self.uploadExercise)

    def initList(self):
        """ Méthode qui initialise les exercices de la liste """
        model = QtGui.QStandardItemModel()

        list_exo = self.data.listProgram.list_exercice
        for exo in list_exo:
            name = str(exo[0].IDE) + " - " + str(exo[0].nom)
            # Create an item with a caption
            item = QtGui.QStandardItem(name)
            # Add the item to the model
            model.appendRow(item)

        # Sort the exercice list
        model.sort(0, QtCore.Qt.AscendingOrder)
        self.jsonFileList.setModel(model)

    def selectSeq(self):
        """ Méthode qui permet d'accéder à la vue MotionTrackingUI """
        motionTracking = MotionTracking(self.data)
        self.setCentralWidget(motionTracking)

    def uploadExercise(self):
        """ Méthode qui télécharge les exercices que le patient doit effectuer """
        options = QFileDialog.Options()
        video_name, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                    "All Files (*);;", options=options)
        if video_name:
            # Create a folder to stock json files
            filepath, filename = os.path.split(video_name)
            if not os.path.exists(path_Exercices_Medecin):
                os.makedirs(path_Exercices_Medecin)
            import shutil
            source = r'' + video_name
            destination = r'' + os.path.join(path_Exercices_Medecin, filename)
            shutil.copyfile(source, destination)


########################################## MOTION TRACKING : VUE 3 #####################################################
class MotionTracking(QMainWindow, MotionTrackingUI.Ui_motionTrackingWindow):
    """
    Classe qui gère les méthodes de l'IHM MotionTrackingUI
    """
    def __init__(self, data, parent=None):
        """ Constructeur de la classe """
        super(MotionTracking, self).__init__(parent)
        self.data = data

        # Name of the folder "export"
        self.currentExportFolderName = self.data.patient.nom + "_" + self.data.patient.prenom
        # Current working directory
        self.currentExportFolderPath = os.path.join(path_Exercices_Patient, self.currentExportFolderName).replace('\\',
                                                                                                                  '/')
        if not os.path.exists(self.currentExportFolderPath):
            # Create the current export folder
            os.makedirs(self.currentExportFolderPath)

        self.setupUi(self)
        self.connectActions()
        self.sendDataButton.setDisabled(True)


    def connectActions(self):
        """ Méthode qui définit les actions des boutons """
        self.previousPageButton.clicked.connect(self.previousPageNav)
        self.startButton.clicked.connect(self.startSequenceOrder)
        self.TestButton.clicked.connect(self.test_video)
        # self.sendDataButton.clicked.connect(self.sendDataOrder)

    def previousPageNav(self):
        """ Méthode qui retourne à la vue MenuUI """
        # Arrêt de la caméra si elle fonctionne
        if (GBV.isCapturing):
            self.stop()
        # Navigate to the Menu page
        else :
            menu = Menu(self.data.patient)
            self.setCentralWidget(menu)

    def startSequenceOrder(self):
        """ Méthode qui lance le programme du patient """
        # Disable the others functionnalities
        self.startButton.setDisabled(True)
        self.TestButton.setDisabled(True)
        self.previousPageButton.setDisabled(True)

        landmarks = self.getLandmarks()
        json_parser = JsonParser()

        with mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5) as holistic:
            # Pour chaque exercice du programme
            for exo in self.data.listProgram.list_exercice:
                dict_medecin = json_parser.readFile(path_Exercices_Medecin, exo[0].fichierJSON)
                # Pour chaque répétition d'exercice
                for i_repetition in range(0, exo[1]):
                    self.labelExercice.setText(str(i_repetition + 1) + " - " + exo[0].nom)
                    dict_patient = {}
                    idRow = 0

                    # On allume la caméra
                    self.start(exo[2])

                    # Tant que la caméra est ouverte
                    for i in range(0, len(dict_medecin)):
                        ret, fra = GBV.cap.read()
                        cv2.flip(fra, +1, fra)
                        frame = fra.copy()
                        pic = cv2.cvtColor(fra, cv2.COLOR_BGR2RGB)
                        pic2 = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

                        try:
                            # Récupération dans un dictionnaire les points mediapipe du patient
                            results = holistic.process(pic)  # Détections du squelette
                            # Récupère les landmark et les stocke dans un dictionnaire de données
                            pose = results.pose_landmarks.landmark
                            pose_row = list(np.array([[landmark.x, landmark.y, landmark.z, landmark.visibility]
                                                      for landmark in pose]).flatten())
                            dict_image = {}
                            for i in range(len(landmarks)):
                                dict_image[landmarks[i]] = pose_row[i]
                            dict_patient[idRow] = dict_image

                            # Affichage des points mediapipe du médecin
                            row = dict_medecin.get(str(idRow))
                            for i in range(0, 33):
                                results.pose_landmarks.landmark[i].x = float(row.get('x' + str(i)))
                                results.pose_landmarks.landmark[i].y = float(row.get('y' + str(i)))
                                results.pose_landmarks.landmark[i].z = float(row.get('z' + str(i)))
                                results.pose_landmarks.landmark[i].visibility = 1
                            # QImage
                            im = QImage(pic2, pic2.shape[1], pic2.shape[0], QImage.Format_RGB888)
                            # Dessine sur l'image
                            mp_drawing.draw_landmarks(pic2, results.pose_landmarks, mp_pose.POSE_CONNECTIONS,
                                                      mp_drawing.DrawingSpec(color=(245, 117, 66), thickness=2,
                                                                             circle_radius=4),
                                                      mp_drawing.DrawingSpec(color=(245, 66, 230), thickness=2,
                                                                             circle_radius=2)
                                                      )
                        except:
                            pass
                        # Met à jour le PixMap du Label
                        pix = QPixmap.fromImage(im)
                        self.cameraStreamFrame.setPixmap(pix)
                        idRow += 1

                        # Si la vidéo est interrompue, on quitte la méthode
                        if cv2.waitKey(10) & 0xFF == ord('q'):
                            break

                    # Stoppe la caméra
                    self.stop()
                    # Création et ajout d'une réalisation d'exercice dans la base
                    filename, date = json_parser.writeFile(self.currentExportFolderPath, exo[0].nom, i_repetition,
                                                           dict_patient)
                    self.data.addProgramRealise(exo[0], date.date(), filename)

                    # Gestion du temps de pause
                    tempsPause = exo[3]
                    self.labelExercice.setText('Temps de pause : ' + str(tempsPause) + ' secondes')
                    self.cameraStreamFrame.setPixmap(QPixmap())
                    while tempsPause:
                        m, s = divmod(tempsPause, 60)
                        min_sec_format = '{:02d}:{:02d}'.format(m, s)
                        self.cameraStreamFrame.setText(min_sec_format)
                        cv2.waitKey(1 * 1000)
                        tempsPause -= 1

        # Modification des labels avec les éléments d'origine
        self.cameraStreamFrame.setPixmap(self.pixmapCamera)
        self.labelExercice.setText("")
        # Enable the others functionnalities
        self.startButton.setDisabled(False)
        self.TestButton.setDisabled(False)
        self.previousPageButton.setDisabled(False)

    def test_video(self):
        """ Méthode qui permet de vérifier que la vidéo fonctionne """
        self.start(1)
        i = 0
        while GBV.cap.isOpened():
            ret, fra = GBV.cap.read()
            cv2.flip(fra, +1, fra)
            pic = cv2.cvtColor(fra, cv2.COLOR_BGR2RGB)

            im = QImage(pic, pic.shape[1], pic.shape[0], QImage.Format_RGB888)

            pix = QPixmap.fromImage(im)
            self.cameraStreamFrame.setPixmap(pix)

            i += 1

            if (i == 200):
                self.stop()
                break

            # Si la vidéo est interrompue, on quitte la méthode
            if cv2.waitKey(10) & 0xFF == ord('q'):
                break

    def getLandmarks(self):
        """ Méthode qui récupère les landmarks de Pose (mediapipe) """
        with mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5):
            landmarks = []
            for val in mp_pose.PoseLandmark:
                landmarks += ['x{}'.format(val.value), 'y{}'.format(val.value), 'z{}'.format(val.value),
                              'v{}'.format(val.value)]
        return landmarks

    def start(self, facteurVitesse):
        """ Méthode qui lance la caméra """
        # Si la caméra fonctionne, on l'arrête
        if (GBV.isCapturing):
            self.stop()
        GBV.cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
        GBV.isCapturing = True
        GBV.timer = QtCore.QTimer()
        GBV.timer.start(int(1000 / (facteurVitesse * 24)))

    def stop(self):
        """ Méthode qui stoppe la caméra """
        print("fin enregistrement")
        GBV.isCapturing = False
        GBV.cap.release()
        GBV.timer.stop()


"""    def sendDataOrder(self):
        # Disable the others functionnalities
        self.previousPageButton.setDisabled(True)
        self.startButton.setDisabled(True)
        self.sendDataButton.setDisabled(True)

        # if self.cam is not None:
        #   self.cam.getWebcamVideoStream().stop()

        # Read data from the csv file before to export
        if self.csvManager is not None:
            self.csvManager.readData()
            self.csvManager.exportToChart()

        # Enable the others functionnalities
        self.previousPageButton.setDisabled(False)"""
