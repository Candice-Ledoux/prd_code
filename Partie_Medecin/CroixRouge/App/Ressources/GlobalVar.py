""" Ce fichier contient les variables globales qui définit les points du squelette mediapipe dans l'application """

import Ressources.config as config
import cv2
import numpy as np

def searchPoint():
    """ Méthode qui permet de repérer les points sur l'image du squelette mediapipe """
    path = config.path_Images + 'squelette2.png'
    img = cv2.imread(path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    for x in range(gray.shape[0]):
        for y in range(gray.shape[1]):
            gray[x][y] += 1
    gray = gray % 2
    # Transforme l'image en binaire
    ret, th = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    # Applique l'algorithme d'érosion
    kernel = np.ones((9, 9), np.uint8)
    im_erode = cv2.erode(th, kernel)
    # Applique l'algorithme des composantes connexes
    nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(im_erode, connectivity=8)
    print(nb_components)
    print(centroids)
    return nb_components, centroids

# Nombre de points sur l'image du squelette mediapipe
nb_points = 23

# Coordonnées des points sur l'image du squelette mediapipe
coord = [[384.02303673, 414.3412023 ],
 [698.05921053, 214.13815789],
 [ 59.98684211, 217.04605263],
 [258.88461538, 229.16025641],
 [496.0130719,  229.25490196],
 [659.19607843, 230.20261438],
 [100.92, 231.04666667],
 [ 34.18709677, 271.96129032],
 [723.04347826, 271.01863354],
 [657.72151899, 272.89873418],
 [ 99.12903226, 274.95483871],
 [177.9869281,  308.90196078],
 [577.13461538, 310.91025641],
 [298.8525641,  444.99358974],
 [458.13907285, 445.01986755],
 [258.99319728, 593.0952381 ],
 [498.21290323, 594.98709677],
 [298.99367089, 751.1835443 ],
 [458.97315436, 752.06040268],
 [432.08053691, 790.97986577],
 [323.06802721, 793.        ],
 [244.03246753, 796.05844156],
 [515.9602649, 799.86754967]]
