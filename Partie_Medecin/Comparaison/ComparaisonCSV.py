import csv
import numpy

i=0

#Initialise les tableaux à zéro
x_erreur = numpy.zeros(34)
y_erreur = numpy.zeros(34)
z_erreur = numpy.zeros(34)

"""Initialise le nouveau fichier csv"""
landmarks = ['Point 0']
for j in range(1, 33):
    landmarks += ['Point '+str(j)]
    with open('coords_diff.csv', mode='w',newline='') as f:
        csv_writer = csv.writer(f, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow(landmarks)

"""Compare le fichier médecin avec le fichier patient et calcule l'erreur relative"""       
#Penser à Changer le nom des fichiers si besoin
with open("coords_docteur.csv", newline ='') as csv_file_docteur,open("coords_patient.csv", newline ='') as csv_file_patient :
    reader1 = csv.DictReader(csv_file_docteur,delimiter=";");
    reader2 = csv.DictReader(csv_file_patient,delimiter=";");
    for row1,row2 in zip(reader1,reader2):
        for i in range(0,33):#33 correspond au nombre de points dans le squelette, à Changer si ce n'est pas le modèle POSE
            
            x_doc = row1['x'+str(i)]
            y_doc = row1['y'+str(i)]
            z_doc = row1['z'+str(i)]
        
            x_pat = row2['x'+str(i)]
            y_pat = row2['y'+str(i)]
            z_pat = row2['z'+str(i)]
            
                
            #Cacul erreur relative   
            x_relatif = abs((float(x_doc) - float(x_pat)))/abs(float(x_pat)) * 100
           
            y_relatif = abs((float(y_doc) - float(y_pat)))/abs(float(y_pat))*100
            z_relatif = abs((float(z_doc) - float(z_pat)))/abs(float(z_pat))*100

            #Si l'erreur relative supérieur à 10% alors incrémente le nombre d'erreur dans le tableau
            if x_relatif <= 10 :
                x_erreur[i] = x_erreur[i]
            else :
                x_erreur[i] =  x_erreur[i]+1
                        
            if y_relatif <= 10 :
                y_erreur[i] = y_erreur[i]
            else :
                y_erreur[i] = y_erreur[i]+1
                        
            if z_relatif <= 10:
                z_erreur[i] = z_erreur[i]
            else:
                z_erreur[i] = z_erreur[i]+1

            
            if i > 33:#33 correspond au nombre de points dans le squelette, à Changer si ce n'est pas le modèle POSE
                break;

"""Ecrit dans le nouveau fichier CSV"""  
with open('coords_diff.csv', mode='a',newline='') as f:
    csv_writer = csv.writer(f, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    csv_writer.writerow(x_erreur)
    csv_writer.writerow(y_erreur)
    csv_writer.writerow(z_erreur)
    print("fin")
    
