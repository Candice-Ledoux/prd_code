import ctypes

from Controller.CotrolIHM import *
from Ressources import config


def main():
    """ Méthode pour lancer l'application """
    app = QtWidgets.QApplication(sys.argv)
    app.setApplicationName("Turbo Rehab'")

    app.setWindowIcon(QtGui.QIcon(config.path_Images + 'icon.png'))

    myappid = 'croix.rouge.app.patient.v2'  # chaîne de caractère aléatoire
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)

    # Navigate to the first vue
    Welcome()
    sys.exit(app.exec_())


# Main
if __name__ == '__main__':
    """ Exécution du Main """
    main()

