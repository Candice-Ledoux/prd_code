""" Ce fichier contient les variables globales qui gère les répertoires utilisés dans l'application """

import os

# Chemin du répertoire courant de l'application
path_App = "App/"

# Chemin du répertoire qui contient toutes les images utilisées dans l'application
path_Images = os.path.join("Ressources/Images/")

# Chemin du répertoire qui contient toutes les vidéos des exercices réalisées par le médecin
path_Videos = "../Exercices_medecin/Videos/"

# Chemin du répertoire qui contient tous les fichiers JSON qui comporte les points mediapipe des exercices réalisés par le médecin
path_Fichiers = "../Exercices_medecin/Fichiers/"