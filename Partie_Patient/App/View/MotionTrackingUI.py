# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MotionTracking.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!

from Ressources import config
from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_motionTrackingWindow(object):
    """
    Classe IHM du programme à suivre
    """
    def setupUi(self, motionTrackingWindow):
        """ Méthode qui initialise les composants de la fenêtre """
        motionTrackingWindow.setObjectName("motionTrackingWindow")
        motionTrackingWindow.setStyleSheet("background-color: rgb(50, 50, 50);")
        self.centralwidget = QtWidgets.QWidget(motionTrackingWindow)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy)
        self.centralwidget.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setSpacing(10)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        #self.verticalLayout.setSpacing(5)
        self.verticalLayout.setObjectName("verticalLayout")
        self.labelExercice = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.labelExercice.setFont(font)
        self.labelExercice.setStyleSheet("color: #FCFCFC;\n"
                                         "text-align: center;")
        self.labelExercice.setText("")
        self.labelExercice.setAlignment(QtCore.Qt.AlignCenter)
        self.labelExercice.setObjectName("labelExercice")
        self.labelExercice.setMaximumSize(1000,50)
        self.verticalLayout.addWidget(self.labelExercice)
        self.cameraStreamFrame = QtWidgets.QLabel(self.centralwidget)
        self.cameraStreamFrame.setStyleSheet("border: 3px solid #FCFCFC;\n"
                                             "color: #FCFCFC;\n"
                                             "border-radius: 10px;\n"
                                             "text-align: center;")
        font.setPointSize(50)
        self.cameraStreamFrame.setFont(font)
        self.cameraStreamFrame.setText("")
        self.pixmapCamera = QtGui.QPixmap(config.path_Images + "photo-camera.png")
        self.cameraStreamFrame.setPixmap(self.pixmapCamera)
        self.cameraStreamFrame.setAlignment(QtCore.Qt.AlignCenter)
        self.cameraStreamFrame.setObjectName("cameraStreamFrame")
        self.verticalLayout.addWidget(self.cameraStreamFrame)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.splitter_3 = QtWidgets.QSplitter(self.centralwidget)
        self.splitter_3.setBaseSize(QtCore.QSize(0, 0))
        self.splitter_3.setOrientation(QtCore.Qt.Horizontal)
        self.splitter_3.setObjectName("splitter_3")
        self.splitter_2 = QtWidgets.QSplitter(self.splitter_3)
        self.splitter_2.setOrientation(QtCore.Qt.Vertical)
        self.splitter_2.setObjectName("splitter_2")
        self.startPict = QtWidgets.QLabel(self.splitter_2)
        self.startPict.setText("")
        self.startPict.setPixmap(QtGui.QPixmap(config.path_Images + "on-button.png"))
        self.startPict.setAlignment(QtCore.Qt.AlignCenter)
        self.startPict.setObjectName("startPict")
        self.backPict = QtWidgets.QLabel(self.splitter_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.backPict.sizePolicy().hasHeightForWidth())
        self.backPict.setSizePolicy(sizePolicy)
        self.backPict.setText("")
        self.backPict.setPixmap(QtGui.QPixmap(config.path_Images + "send.png"))
        self.backPict.setAlignment(QtCore.Qt.AlignCenter)
        self.backPict.setObjectName("backPict")
        self.sendPict = QtWidgets.QLabel(self.splitter_2)
        self.sendPict.setText("")
        self.sendPict.setPixmap(QtGui.QPixmap(config.path_Images + "back.png"))
        self.sendPict.setAlignment(QtCore.Qt.AlignCenter)
        self.sendPict.setObjectName("sendPict")
        self.splitter = QtWidgets.QSplitter(self.splitter_3)
        self.splitter.setOrientation(QtCore.Qt.Vertical)
        self.splitter.setObjectName("splitter")
        self.startButton = QtWidgets.QPushButton(self.splitter)
        font = QtGui.QFont()
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.startButton.setFont(font)
        self.startButton.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.startButton.setStyleSheet("QPushButton {\n"
                                       "    border: 3px solid #ffffff;\n"
                                       "    color: #ffffff;\n"
                                       "    border-radius: 10px;\n"
                                       "}\n"
                                       "\n"
                                       "QPushButton:hover {\n"
                                       "    background-color: #EAECEE;\n"
                                       "    color: black;\n"
                                       "    border-color: #6a6ea9;\n"
                                       "}\n"
                                       "\n"
                                       "QPushButton:disabled {\n"
                                       "    color: rgb(130, 130, 130);\n"
                                       "    border-color: rgb(130, 130, 130);\n"
                                       "}\n"
                                       "")
        self.startButton.setObjectName("startButton")
        self.sendDataButton = QtWidgets.QPushButton(self.splitter)
        font = QtGui.QFont()
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.sendDataButton.setFont(font)
        self.sendDataButton.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.sendDataButton.setStyleSheet("QPushButton {\n"
                                          "    border: 3px solid #ffffff;\n"
                                          "    color: #ffffff;\n"
                                          "    border-radius: 10px;\n"
                                          "}\n"
                                          "\n"
                                          "QPushButton:hover {\n"
                                          "    background-color: #EAECEE;\n"
                                          "    color: black;\n"
                                          "    border-color: #6a6ea9;\n"
                                          "}\n"
                                          "\n"
                                          "QPushButton:disabled {\n"
                                          "    color: rgb(130, 130, 130);\n"
                                          "    border-color: rgb(130, 130, 130);\n"
                                          "}")
        self.sendDataButton.setObjectName("sendDataButton")
        self.previousPageButton = QtWidgets.QPushButton(self.splitter)
        font = QtGui.QFont()
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.previousPageButton.setFont(font)
        self.previousPageButton.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.previousPageButton.setStyleSheet("QPushButton {\n"
                                              "    border: 3px solid #ffffff;\n"
                                              "    color: #ffffff;\n"
                                              "    border-radius: 10px;\n"
                                              "}\n"
                                              "\n"
                                              "QPushButton:hover {\n"
                                              "    background-color: #EAECEE;\n"
                                              "    color: black;\n"
                                              "    border-color: #6a6ea9;\n"
                                              "}\n"
                                              "\n"
                                              "QPushButton:disabled {\n"
                                              "    color: rgb(130, 130, 130);\n"
                                              "    border-color: rgb(130, 130, 130);\n"
                                              "}")
        self.previousPageButton.setObjectName("previousPageButton")
        self.TestButton = QtWidgets.QPushButton(self.splitter)
        self.TestButton.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.TestButton.setFont(font)
        self.TestButton.setStyleSheet("QPushButton {\n"
                                      "    border: 3px solid #ffffff;\n"
                                      "    color: #ffffff;\n"
                                      "    border-radius: 10px;\n"
                                      "}\n"
                                      "\n"
                                      "QPushButton:hover {\n"
                                      "    background-color: #EAECEE;\n"
                                      "    color: black;\n"
                                      "    border-color: #6a6ea9;\n"
                                      "}\n"
                                      "\n"
                                      "QPushButton:disabled {\n"
                                      "    color: rgb(130, 130, 130);\n"
                                      "    border-color: rgb(130, 130, 130);\n"
                                      "}")
        self.TestButton.setObjectName("TestButton")
        self.horizontalLayout.addWidget(self.splitter_3)
        self.horizontalLayout.setStretch(0, 2)
        self.splitter_3.raise_()
        self.cameraStreamFrame.raise_()
        motionTrackingWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(motionTrackingWindow)
        QtCore.QMetaObject.connectSlotsByName(motionTrackingWindow)


    def retranslateUi(self, motionTrackingWindow):
        """ Méthode de traduction, qui modifie les textes affichés sur la fenêtre """
        _translate = QtCore.QCoreApplication.translate
        motionTrackingWindow.setWindowTitle(_translate("motionTrackingWindow", "Turbo Rehab\'"))
        self.startButton.setText(_translate("motionTrackingWindow", "Démarrer"))
        self.sendDataButton.setText(_translate("motionTrackingWindow", "Envoyer les données"))
        self.previousPageButton.setText(_translate("motionTrackingWindow", "Page précédente"))
        self.TestButton.setText(_translate("motionTrackingWindow", "Test avec vidéo"))


