from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import *


######################################### POPUP #############################################

class PopUp(QMainWindow):
    """
    Classe de gestion des popups
    """
    def __init__(self, parent=None):
        """ Consructeur de la classe """
        super(PopUp, self).__init__(parent)

    def champsVide(self, string):
        """ Vérifie que les champs LineEdit ne sont pas vides et envoie un popup si l'un d'eux est vide """
        for str in string:
            if len(str) == 0:
                qm = QtWidgets.QMessageBox()
                qm.information(self, 'Attention', "Un des champs est vide.", qm.Ok)
                return False
        return True

    def mdpCorrect(self, mdp):
        """ Vérifie que le mot de passe du patient comporte au moins 8 caractères et envoie un popup si ce n'est pas le cas """
        if len(mdp) < 8:
            qm = QtWidgets.QMessageBox()
            qm.information(self, 'Attention', "Le mot de passe doit contenir au moins 8 caractères.", qm.Ok)
            return False
        else:
            return True

    def selectLine(self, list):
        """ Vérifie qu'une ligne du tableau est sélectionnée et envoie un popup si ce n'est pas le cas """
        if list == []:
            qm = QtWidgets.QMessageBox()
            qm.information(self, 'Attention', 'Vous devez cliquer sur une ligne du tableau.', qm.Ok)
            return False
        else:
            return True

    def selectPoint(self, points):
        """ Vérifie qu'au moins un point du squelette est sélectionné et envoie un popup si ce n'est pas le cas """
        if len(points) == 0:
            qm = QtWidgets.QMessageBox()
            qm.information(self, 'Attention', 'Vous devez sélectonner au moins un point du squelette.', qm.Ok)
            return False
        else:
            return True

    def captureIsPresent(self, dict_mediapipe):
        """ Vérifie qu'une capture d'exercice a été réalisée et envoie un popup si ce n'est pas le cas """
        if dict_mediapipe == {}:
            qm = QtWidgets.QMessageBox()
            qm.information(self, 'Attention', "Vous devez réaliser une capture d'exercice.", qm.Ok)
            return False
        else:
            return True

    def addElement(self, string):
        """
        Affiche un message lors de l'ajout d'un élement
        Retourne Faux si l'utilisateur répond Non et True si la réponse est Oui
        """
        msg = ""
        for str in string:
            msg += str + " "
        qm = QtWidgets.QMessageBox()
        reply = qm.question(self, 'Continuer ?', 'Voulez vraiment ajouter ' + msg + "?", qm.Yes, qm.No)
        if reply == qm.Yes:
            return True
        else:
            return False



######################################### POPUP_PROGRAM #############################################

class PopUpProgram(QDialog):
    """
    Classe qui gère le popup utilisé dans l'ajout d'un proramme
    """
    def __init__(self, parent):
        """ Consructeur de la classe """
        super().__init__(parent)

        self.nbRepetition = ""
        self.vitesse = ""
        self.tempsPause = ""

        # Vertical Layout
        self.verticalLayout = QtWidgets.QVBoxLayout(self)
        self.verticalLayout.setObjectName("verticalLayout")

        # Horrizontal Layout
        self.horrizontalLayout_1 = QtWidgets.QHBoxLayout(self)
        self.horrizontalLayout_1.setObjectName("horrizontalLayout_1")

        self.horrizontalLayout_2 = QtWidgets.QHBoxLayout(self)
        self.horrizontalLayout_2.setObjectName("horrizontalLayout_2")

        self.horrizontalLayout_3 = QtWidgets.QHBoxLayout(self)
        self.horrizontalLayout_3.setObjectName("horrizontalLayout_3")

        self.horrizontalLayout_4 = QtWidgets.QHBoxLayout(self)
        self.horrizontalLayout_4.setObjectName("horrizontalLayout_4")

        # Nb Répétitions
        self.label_nbRepetition = QtWidgets.QLabel()
        self.label_nbRepetition.setStyleSheet("font: 12pt \"MS Shell Dlg 2\";")
        self.label_nbRepetition.setObjectName("label_nbRepetition")

        self.lineEdit_nbRepetition = QtWidgets.QLineEdit()
        self.lineEdit_nbRepetition.setStyleSheet("font: 10pt \"MS Shell Dlg 2\";")
        self.lineEdit_nbRepetition.setObjectName("lineEdit_nbRepetition")

        self.horrizontalLayout_1.addWidget(self.label_nbRepetition)
        self.horrizontalLayout_1.addWidget(self.lineEdit_nbRepetition)
        self.verticalLayout.addLayout(self.horrizontalLayout_1)

        # Vitesse
        self.label_vitesse = QtWidgets.QLabel()
        self.label_vitesse.setStyleSheet("font: 12pt \"MS Shell Dlg 2\";")
        self.label_vitesse.setObjectName("label_vitesse")

        self.lineEdit_vitesse = QtWidgets.QLineEdit()
        self.lineEdit_vitesse.setStyleSheet("font: 10pt \"MS Shell Dlg 2\";")
        self.lineEdit_vitesse.setObjectName("lineEdit_vitesse")

        self.horrizontalLayout_2.addWidget(self.label_vitesse)
        self.horrizontalLayout_2.addWidget(self.lineEdit_vitesse)
        self.verticalLayout.addLayout(self.horrizontalLayout_2)

        # Temps de pause
        self.label_tempsPause = QtWidgets.QLabel()
        self.label_tempsPause.setStyleSheet("font: 12pt \"MS Shell Dlg 2\";")
        self.label_tempsPause.setObjectName("label_tempsPause")

        self.label_min = QtWidgets.QLabel()
        self.label_min.setStyleSheet("font: 12pt \"MS Shell Dlg 2\";")
        self.label_min.setObjectName("label_min")

        self.label_sec = QtWidgets.QLabel()
        self.label_sec.setStyleSheet("font: 12pt \"MS Shell Dlg 2\";")
        self.label_sec.setObjectName("label_sec")

        self.lineEdit_min = QtWidgets.QLineEdit()
        self.lineEdit_min.setStyleSheet("font: 10pt \"MS Shell Dlg 2\";")
        self.lineEdit_min.setObjectName("lineEdit_min")

        self.lineEdit_sec = QtWidgets.QLineEdit()
        self.lineEdit_sec.setStyleSheet("font: 10pt \"MS Shell Dlg 2\";")
        self.lineEdit_sec.setObjectName("lineEdit_sec")

        self.horrizontalLayout_3.addWidget(self.label_tempsPause)
        self.horrizontalLayout_3.addWidget(self.lineEdit_min)
        self.horrizontalLayout_3.addWidget(self.label_min)
        self.horrizontalLayout_3.addWidget(self.lineEdit_sec)
        self.horrizontalLayout_3.addWidget(self.label_sec)
        self.verticalLayout.addLayout(self.horrizontalLayout_3)

        # Boutons
        self.button_Ok = QtWidgets.QPushButton()
        self.button_Ok.setObjectName("button_Ok")

        self.button_Cancel = QtWidgets.QPushButton()
        self.button_Cancel.setObjectName("button_Cancel")

        self.horrizontalLayout_4.addWidget(self.button_Ok)
        self.horrizontalLayout_4.addWidget(self.button_Cancel)
        self.verticalLayout.addLayout(self.horrizontalLayout_4)

        # Action des boutons
        self.button_Ok.clicked.connect(lambda: self.Click_button_Ok())
        self.button_Cancel.clicked.connect(lambda: self.Click_button_Cancel())

        # Affichage des textes des labels
        self.retranslateUi()

        self.setWindowFlags(
            QtCore.Qt.Window |
            QtCore.Qt.CustomizeWindowHint |
            QtCore.Qt.WindowTitleHint |
            QtCore.Qt.WindowCloseButtonHint |
            QtCore.Qt.WindowStaysOnTopHint
        )

        self.exec_()


    def retranslateUi(self):
        """ Méthode de traduction, qui modifie les textes affichés sur la fenêtre """
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("Popup Program", "Application Croix Rouge - Médecin"))
        self.label_nbRepetition.setText(_translate("Popup Program", "Nombre de répétitions"))
        self.label_vitesse.setText(_translate("Popup Program", "Vitesse"))
        self.label_tempsPause.setText(_translate("Popup Program", "Temps de pause"))
        self.label_min.setText(_translate("Popup Program", "min"))
        self.label_sec.setText(_translate("Popup Program", "sec"))
        self.button_Ok.setText(_translate("Popup Program", "OK"))
        self.button_Cancel.setText(_translate("Popup Program", "Annuler"))

    def Click_button_Ok(self):
        """
        Méthode utilisée lors du clic sur le bouton Ok
        Ferme le popup et met à jour les champs self.nbRepetition, self.vitesse, self.tempsPause
        """
        self.nbRepetition = self.lineEdit_nbRepetition.text()
        self.vitesse = self.lineEdit_vitesse.text()
        min = str(self.lineEdit_min.text())
        sec = str(self.lineEdit_sec.text())
        pop = PopUp(self)
        if pop.champsVide([self.nbRepetition, self.vitesse, min, sec]) :
            self.tempsPause = (int(self.lineEdit_min.text()) * 60 + int(self.lineEdit_sec.text()))
            self.close()


    def Click_button_Cancel(self):
        """
        Méthode utilisée lors du clic sur le bouton Annuler
        Ferme le popup
        """
        self.nbRepetition = ""
        self.vitesse = ""
        self.tempsPause = ""
        self.close()


