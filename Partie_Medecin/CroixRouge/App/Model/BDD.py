import mysql.connector
from mysql.connector import Error
from Model.Model import *

class BDD():
    """
    Classe qui permet d'effectuer des requêtes sur la base de données
    """

    ####################### CONNEXION ##############################

    def Connexion(self):
        """
        Méthode permettant de se connecter à la base de données
        """
        try:
            connexion = mysql.connector.connect(
                host="localhost",
                port=3306,
                database="croix_rouge",
                user="root",
                password=""
            )
            curseur = connexion.cursor()
            print("Connexion ouverte")

        except Error as e:
            print(e)

        return connexion, curseur


    def FinConnexion(self, connexion, curseur):
        """
        Méthode permettant de fermer la connexion à la base de données
        """
        if (connexion.is_connected()):
            connexion.close()
            curseur.close()
            print("Connexion fermée")

    def CreerDataBase(self):
        """
        Méthode permettant de créer la base de données
        """
        try:
            connexion = mysql.connector.connect(
                host="localhost",
                port=3306,
                user="root",
                password=""
            )
            #print(connexion != None)
            curseur = connexion.cursor()
            print("Connexion ouverte")

            requete = "CREATE DATABASE IF NOT EXISTS `croix_rouge` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;" \
                      "USE `croix_rouge`;" \
                    "CREATE TABLE IF NOT EXISTS `affectation` ( \
                      `IDP` int(11) NOT NULL, \
                      `IDE` int(11) NOT NULL, \
                      `NbRepetition` int(11) NOT NULL, \
                      `Vitesse` float NOT NULL, \
                      `TempsPause` int(11) NOT NULL, \
                      PRIMARY KEY (`IDP`,`IDE`) \
                    ) ENGINE=MyISAM DEFAULT CHARSET=latin1;" \
                    "CREATE TABLE IF NOT EXISTS `categorie` ( \
                      `NomCategorie` varchar(255) NOT NULL, \
                      `Point` varchar(255) NOT NULL, \
                      PRIMARY KEY (`NomCategorie`) \
                    ) ENGINE=MyISAM DEFAULT CHARSET=latin1;" \
                    "CREATE TABLE IF NOT EXISTS `exercice` ( \
                      `IDE` int(11) NOT NULL AUTO_INCREMENT, \
                      `Nom` varchar(255) NOT NULL, \
                      `FichierJsonM` varchar(255) NOT NULL,\
                      `Video` varchar(255) NOT NULL, \
                      `Description` varchar(256) DEFAULT NULL, \
                      `NomCategorie` varchar(255) NOT NULL, \
                      PRIMARY KEY (`IDE`), \
                      KEY `FOREIGN` (`NomCategorie`) \
                    ) ENGINE=MyISAM DEFAULT CHARSET=latin1;" \
                    "CREATE TABLE IF NOT EXISTS `patient` ( \
                      `IDP` int(11) NOT NULL AUTO_INCREMENT, \
                      `Nom` varchar(255) NOT NULL, \
                      `Prenom` varchar(255) NOT NULL, \
                      `Mot_de_passe` varchar(256) NOT NULL, \
                      PRIMARY KEY (`IDP`) \
                    ) ENGINE=MyISAM DEFAULT CHARSET=latin1;" \
                    "CREATE TABLE IF NOT EXISTS `realisation` ( \
                      `IDR` int(11) NOT NULL AUTO_INCREMENT, \
                      `Date` date NOT NULL, \
                      `FichierJsonP` varchar(255) NOT NULL, \
                      `IDP` int(11) NOT NULL, \
                      `IDE` int(11) NOT NULL, \
                      PRIMARY KEY (`IDR`), \
                      KEY `FOREIGN` (`IDP`) USING BTREE, \
                      KEY `FOREIGN2` (`IDE`) \
                    ) ENGINE=MyISAM DEFAULT CHARSET=latin1;"

            curseur.execute(requete, multi=True)
            connexion.commit()

        except Error as e:
            print(e)

        finally:
            self.FinConnexion(connexion, curseur)



    ####################### PATIENT ##############################

    def insertPatient(self, patient):
        """
        Méthode permettant d'insérer un patient dans la base de données
        """
        try:
            connexion, curseur = self.Connexion()
            request = "INSERT INTO patient (IDP, Nom, Prenom, Mot_de_passe) VALUES (0, "  +"'"+ patient.nom \
                      +"','"+ patient.prenom +"','"+ patient.mdp +"' ); "
            curseur.execute(request)
            connexion.commit()

        except Error as e:
            print(e)

        finally:
            self.FinConnexion(connexion, curseur)


    def selectPatient(self):
        """
        Méthode permettant de sélectionner tous les patients de la base
        Retourne une liste d'objets patient
        """
        try:
            list_patient = []
            connexion, curseur = self.Connexion()
            request = "Select * from patient; "
            curseur.execute(request)
            patients = curseur.fetchall()
            for p in patients:
                pat = Patient(p[0], p[1], p[2], p[3])
                list_patient.append(pat)

        except Error as e:
            print(e)

        finally:
            self.FinConnexion(connexion, curseur)
            return list_patient

    def selectPatientByID(self, id):
        """
        Méthode permettant de sélectionner un patient par son ID dans la base
        Retourne un objet patient
        """
        try:
            list_patient = []
            connexion, curseur = self.Connexion()
            request = "Select * from patient where (IDP = " + str(id) + "); "
            curseur.execute(request)
            patients = curseur.fetchall()
            for p in patients:
                pat = Patient(p[0], p[1], p[2], p[3])
                list_patient.append(pat)

        except Error as e:
            print(e)

        finally:
            self.FinConnexion(connexion, curseur)
            return list_patient


    ####################### EXERCICE ##############################

    def insertExercice(self, exercice):
        """
        Méthode permettant d'insérer un exercice dans la base de données
        """
        try:
            connexion, curseur = self.Connexion()
            request = "INSERT INTO exercice (IDE, Nom, FichierJsonM, Video, Description, NomCategorie) VALUES (0, " \
                      + "'" + exercice.nom + "','" + exercice.fichierJSON + "','" + exercice.video + "','" + \
                      exercice.description + "','" + exercice.categorie.nom + "' ); "
            curseur.execute(request)
            connexion.commit()

        except Error as e:
            print(e)

        finally:
            self.FinConnexion(connexion, curseur)


    def selectExercice(self):
        """
        Méthode permettant de sélectionner tous les exercices de la base
        Retourne une liste d'objets exercice
        """
        try:
            list_exercice = []
            connexion, curseur = self.Connexion()
            request = "Select e.IDE, e.Nom, e.FichierJsonM, e.Video, e.Description, e.NomCategorie, " \
                      "c.point from exercice e, categorie c where (e.NomCategorie = c.NomCategorie) " \
                      "order by e.NomCategorie; "
            curseur.execute(request)
            exercices = curseur.fetchall()
            for e in exercices:
                c = Categorie(e[5], e[6])
                ex = Exercice(e[0], e[1], e[2], e[3], e[4], c)
                list_exercice.append(ex)

        except Error as e:
            print(e)

        finally:
            self.FinConnexion(connexion, curseur)
            return list_exercice


    ####################### CATEGORIE ##############################

    def insertCategorie(self, categorie):
        """
        Méthode permettant d'insérer une catégorie dans la base de données
        """
        try:
            connexion, curseur = self.Connexion()
            request = "INSERT INTO categorie (NomCategorie, Point) VALUES ('" + categorie.nom \
                      + "','" + categorie.points + "' ); "
            curseur.execute(request)
            connexion.commit()

        except Error as e:
            print(e)

        finally:
            self.FinConnexion(connexion, curseur)


    def selectCategorie(self):
        """
        Méthode permettant de sélectionner toutes les catégories d'exercices de la base
        Retourne une liste d'objets categorie
        """
        try:
            list_categorie = []
            connexion, curseur = self.Connexion()
            request = "Select * from categorie Order By NomCategorie; "
            curseur.execute(request)
            categories = curseur.fetchall()
            for c in categories:
                cat = Categorie(c[0], c[1])
                list_categorie.append(cat)

        except Error as e:
            print(e)

        finally:
            self.FinConnexion(connexion, curseur)
            return list_categorie


    ####################### PROGRAM ##############################

    def insertAffectation(self, patient, exercice, nbRepetition, vitesse, tempsPause):
        """
        Méthode permettant d'insérer une affectation dans la base de données
        """
        try:
            connexion, curseur = self.Connexion()
            request = "INSERT INTO affectation (IDP, IDE, NbRepetition, Vitesse, TempsPause) VALUES (" + str(patient.IDP) \
                      + "," + str(exercice.IDE) + "," + str(nbRepetition) + "," + str(vitesse) + "," + str(tempsPause) + "); "
            curseur.execute(request)
            connexion.commit()

        except Error as e:
            print(e)

        finally:
            self.FinConnexion(connexion, curseur)


    def deleteAffectation(self, patient, exercice):
        """
        Méthode permettant de supprimer une affectation dans la base de données
        """
        try:
            connexion, curseur = self.Connexion()
            request = "DELETE FROM affectation WHERE(IDP =" + str(patient.IDP) + " AND IDE =" + str(exercice.IDE) + "); "
            curseur.execute(request)
            connexion.commit()

        except Error as e:
            print(e)

        finally:
            self.FinConnexion(connexion, curseur)


    def selectAffectationByPatient(self):
        """
        Méthode permettant de sélectionner toutes les affectations d'exercices triées par patient de la base
        Retourne une liste d'objets program
        """
        try:
            list_prog = []
            connexion, curseur = self.Connexion()
            request = "SELECT p.*, e.*, c.Point, a.NbRepetition, a.Vitesse, a.TempsPause FROM patient p, exercice e, " \
                      "categorie c, affectation a WHERE(p.IDP = a.IDP AND e.IDE = a.IDE AND e.NomCategorie = " \
                      "c.NomCategorie) GROUP BY p.IDP, e.IDE"
            curseur.execute(request)
            affectations = curseur.fetchall()

            ID_patient = -1
            count = 0
            prog = []
            # Pour chaque ligne dans affectations
            for a in affectations:
                # On récupère les valeurs pour créer un patient, une categorie et un exercice
                pat = Patient(a[0], a[1], a[2], a[3])
                cat = Categorie(a[9], a[10])
                ex = Exercice(a[4], a[5], a[6], a[7], a[8], cat)
                # Si l'identifiant du patient est différent du patient précédent
                if ID_patient != a[0]:
                    # Si le programme de l'ancien patient n'est pas vide, on l'ajoute dans la liste des programmes
                    if prog != []:
                        list_prog.append(prog)
                    # On crée un nouveau programme pour le nouveau patient
                    prog = Program(pat, [])
                    # On ajoute l'exercice au programme du nouveau patient
                    prog.addExercice(ex, a[11], a[12], a[13])
                    # On met à jour l'identifiant de l'ancien patient avec celui du nouveau
                    ID_patient = a[0]
                # Sinon, si l'identifiant du patient est identique au patient précédent, on ajoute un exercice à son programme
                else:
                    prog.addExercice(ex, a[11], a[12], a[13])
                count += 1
                # S'il s'agit de la dernière ligne dans affectations, on ajoute le programme dans la liste de tous les programmes
                if len(affectations) == count :
                    list_prog.append(prog)

        except Error as e:
            print(e)

        finally:
            self.FinConnexion(connexion, curseur)
            return list_prog
