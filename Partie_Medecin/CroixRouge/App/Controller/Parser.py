import json
import datetime
import cv2
import os

import Ressources.config as config


class JsonParser():
    """
    Classe qui permet de lire et écrire des fichiers JSON
    """
    def writeFile(self, name, dict_mediapipe):
        """
        Méthode qui permet de créer un fichier JSON et d'écrire dans celui-ci
        Retourne le nom du fichier créé
        """
        date = datetime.datetime.now()
        filename = name + '_' + str(date.year) + '-' + str(date.month) + '-' + str(date.day) + \
                   '_' + str(date.hour) + '-' + str(date.minute) + '.json'
        rep_cour = os.getcwd()
        os.chdir(config.path_Fichiers)
        with open(filename, 'w') as fichier:
            json.dump(dict_mediapipe, fichier)
        os.chdir(rep_cour)
        return filename

    def readFile(self, filename):
        """
        Méthode qui permet de lire un fichier JSON grâce à son nom
        Retourne un dictionnaire qui correspond aux données stockées dans le fichier
        """
        rep_cour = os.getcwd()
        os.chdir(config.path_Fichiers)
        with open(filename, 'r') as fichier:
            dict_mediapipe = json.load(fichier)
        os.chdir(rep_cour)
        return dict_mediapipe

class VideoParser():
    """
    Classe qui permet de créer une vidéo
    """
    def writeFile(self, name, fps, listImages):
        """
        Méthode qui permet de créer un fichier vidéo AVI et d'écrire dans celui-ci avec une liste d'images
        Retourne le nom du fichier créé
        """
        date = datetime.datetime.now()
        filename = name + '_' + str(date.year) + '-' + str(date.month) + '-' + str(date.day) + \
                   '_' + str(date.hour) + '-' + str(date.minute) + '.avi'
        rep_cour = os.getcwd()
        os.chdir(config.path_Videos)
        fourcc = cv2.VideoWriter_fourcc(*'MJPG')
        self.file = cv2.VideoWriter(filename, fourcc, fps, (640, 480))
        for image in listImages:
            image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
            self.file.write(image)
        os.chdir(rep_cour)
        return filename

