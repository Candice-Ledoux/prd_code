""" Ce fichier contient les variables globales qui gère le status de la vidéo dans l'application """

# Booléen qui indique si la capture vidéo est en cours
isCapturing = False

# cv2.VideoCapture(0) qui lance la capture ou qui la stoppe
cap = None

# QTimer qui lancer un timer ou qui le stoppeq
timer = None