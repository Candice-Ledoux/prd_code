# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Welcome.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from Ressources import config
from PyQt5 import QtWidgets, QtGui, QtCore

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig)

class Ui_welcomeWindow(object):
    """
    Classe IHM de la connexion à l'application
    """
    def setupUi(self, welcomeWindow):
        """ Méthode qui initialise les composants de la fenêtre """
        welcomeWindow.setObjectName(_fromUtf8("welcomeWindow"))
        welcomeWindow.resize(1309, 900)
        welcomeWindow.setStyleSheet(_fromUtf8("background-color: rgb(255, 255, 255);"))
        self.centralwidget = QtWidgets.QWidget(welcomeWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.splitter = QtWidgets.QSplitter(self.centralwidget)
        self.splitter.setOrientation(QtCore.Qt.Vertical)
        self.splitter.setObjectName(_fromUtf8("splitter"))
        self.logoCR = QtWidgets.QLabel(self.splitter)
        self.logoCR.setStyleSheet(_fromUtf8(""))
        self.logoCR.setText(_fromUtf8(""))
        self.logoCR.setPixmap(QtGui.QPixmap(_fromUtf8(config.path_Images + "croix-rouge.png")))
        self.logoCR.setAlignment(QtCore.Qt.AlignCenter)
        self.logoCR.setObjectName(_fromUtf8("logoCR"))
        self.line = QtWidgets.QFrame(self.splitter)
        self.line.setStyleSheet(_fromUtf8("color: rgb(50, 50, 50);"))
        self.line.setFrameShadow(QtWidgets.QFrame.Plain)
        self.line.setLineWidth(10)
        self.line.setMidLineWidth(0)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setObjectName(_fromUtf8("line"))
        self.startApp = QtWidgets.QLabel(self.splitter)
        font = QtGui.QFont()
        font.setPointSize(48)
        font.setBold(True)
        font.setWeight(75)
        self.startApp.setFont(font)
        self.startApp.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.startApp.setStyleSheet(_fromUtf8("QLabel:hover{\n"
"    border-radius: 10px;\n"
"    border: 2px solid red;\n"
"    margin: 10px;\n"
"    padding: 20px;\n"
"}"))
        self.startApp.setText(_fromUtf8(""))
        self.startApp.setPixmap(QtGui.QPixmap(_fromUtf8(config.path_Images + "logo.png")))
        self.startApp.setAlignment(QtCore.Qt.AlignCenter)
        self.startApp.setObjectName(_fromUtf8("startApp"))
        self.verticalLayout.addWidget(self.splitter)
        welcomeWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(welcomeWindow)
        QtCore.QMetaObject.connectSlotsByName(welcomeWindow)

    def retranslateUi(self, welcomeWindow):
        """ Méthode de traduction, qui modifie les textes affichés sur la fenêtre """
        welcomeWindow.setWindowTitle(_translate("welcomeWindow", "Turbo Rehab\'", None))

