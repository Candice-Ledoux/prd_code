import unittest

from Partie_Medecin.CroixRouge.Model.model import *
from Partie_Medecin.CroixRouge.Model.BDD import *


class Tests_BDD(unittest.TestCase):
    """
    Classe de test des requêtes sur la base de données
    """

    @classmethod
    def setUpClass(self):
        self.patient = patient(0, 'NomP', 'PrenomP', 'passwordP')
        self.categorie = categorie('categorie', 'x12,y12,z12,v12')
        self.exercice = exercice(0, 'exo', 'exo.json', 'exo.avi', 'description exo', self.categorie)
        self.nbRepetition = 3
        self.vitesse = 1
        self.tempsPause = 30
        self.bdd = BDD()


    ####################### PATIENT ##############################

    def insertPatient_test(self):
        """
        Méthode permettant d'insérer un patient dans la base de données
        """
        list_patient_1 = self.bdd.selectPatient()
        self.bdd.insertPatient(self.patient)
        list_patient_2 = self.bdd.selectPatient()
        self.assertEqual(len(list_patient_1) + 1, len(list_patient_2), "Patient non ajouté")


    def selectPatientByID_test(self):
        """
        Méthode permettant de sélectionner un patient par son ID dans la base
        Retourne un objet patient
        """
        pat = self.bdd.selectPatientByID(1)
        self.assertTrue(pat != None, "Pas de sélection de patient d'identifiant 1")


    ####################### EXERCICE ##############################

    def insertExercice_test(self):
        """
        Méthode permettant d'insérer un exercice dans la base de données
        """
        list_exercice_1 = self.bdd.selectExercice()
        self.bdd.insertExercice(self.exercice)
        list_exercice_2 = self.bdd.selectExercice()
        self.assertEqual(len(list_exercice_1) + 1, len(list_exercice_2), "Exercice non ajouté")


    ####################### CATEGORIE ##############################

    def insertCategorie_test(self):
        """
        Méthode permettant d'insérer une catégorie dans la base de données
        """
        list_categorie_1 = self.bdd.selectCategorie()
        self.bdd.insertCategorie(self.categorie)
        list_categorie_2 = self.bdd.selectCategorie()
        self.assertEqual(len(list_categorie_1) + 1, len(list_categorie_2), "Catégorie non ajoutée")


    ####################### PROGRAM ##############################

    def get_Patient_Exercice(self):
        pat = self.bdd.selectPatient()[-1]
        list_exo = self.bdd.selectExercice()
        exo = None
        for ex in list_exo:
            if ex.categorie.nom == "categorie":
                exo = ex
        return pat, exo

    def insertAffectation_test(self):
        """
        Méthode permettant d'insérer une affectation dans la base de données
        """
        pat, exo = self.get_Patient_Exercice()
        self.bdd.insertAffectation(pat, exo, self.nbRepetition, self.vitesse, self.tempsPause)
        prog = None
        list_program = self.bdd.selectAffectationByPatient()
        for program in list_program:
            if program.patient.IDP == pat.IDP:
                prog = program
                break
        self.assertIsNotNone(prog, "Programme non ajouté")

    def deleteAffectation_test(self):
        """
        Méthode permettant de supprimer une affectation dans la base de données
        """
        pat, exo = self.get_Patient_Exercice()
        self.bdd.deleteAffectation(pat, exo)
        prog = None
        list_program = self.bdd.selectAffectationByPatient()
        for program in list_program:
            if program.patient.IDP == pat.IDP:
                prog = program
                break
        self.assertIsNone(prog, "Programme non supprimé")


    def test_BDD(self):
        self.insertPatient_test()
        self.selectPatientByID_test()
        self.insertCategorie_test()
        self.insertExercice_test()
        self.insertAffectation_test()
        self.deleteAffectation_test()



def suite():
    suite = unittest.TestSuite()
    suite.addTest(Tests_BDD('test_BDD'))
    return suite

if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())
