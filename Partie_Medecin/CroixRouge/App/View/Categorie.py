from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtGui import *

import Ressources.config as config
from Controller.ControlData import ControlData
from Model.Model import Categorie
from Controller.Squelette_categorie import Squelette
from View.PopUp import PopUp
import View.Menu as menu

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class Ui_MainWindow(object):
    """
    Classe IHM pour l'ajout d'une catégorie
    """
    def setupUi(self, MainWindow):
        """
        Méthode qui initialise les composants de la fenêtre
        """
        MainWindow.setObjectName("MainWindow")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.all = QtWidgets.QWidget(self.centralwidget)
        self.all.setStyleSheet("QWidget#all{border-radius: 20px;\n"
                               "blur-raduis:5px;\n"
                               "border-style: solid;\n"
                               "border-color: black;\n"
                               "border-width: 2px;\n"
                               "pading:50px;\n"
                               "}")
        self.all.setObjectName("all")
        self.tab = QtWidgets.QGridLayout(self.all)
        self.tab.setContentsMargins(0, 0, 0, 20)
        self.tab.setSpacing(0)
        self.tab.setObjectName("tab")
        self.head = QtWidgets.QWidget(self.all)
        self.head.setStyleSheet("/*QWidget#head{\n"
                                "    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(255, 255, 255, 255), stop:1 rgba(192, 192, 192, 255));\n"
                                "    border-style: solid;\n"
                                "border-color: black;\n"
                                "border-width: 2px;\n"
                                "border-radius: 20px;\n"
                                "}*/")
        self.head.setObjectName("head")
        self.gridLayout_10 = QtWidgets.QGridLayout(self.head)
        self.gridLayout_10.setObjectName("gridLayout_10")

        # Label Ajout d'une catégorie d'exercices
        self.label_3 = QtWidgets.QLabel(self.head)
        self.label_3.setStyleSheet("font: 75 16pt \"MS Shell Dlg 2\";")
        self.label_3.setObjectName("label_3")
        self.gridLayout_10.addWidget(self.label_3, 0, 0, 1, 1, QtCore.Qt.AlignHCenter)

        self.tab.addWidget(self.head, 0, 0, 1, 1)
        self.base = QtWidgets.QWidget(self.all)
        self.base.setStyleSheet(
            "QWidget#base{background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(108, 219, 215, 255), stop:1 rgba(78, 144, 185, 255));\n"
            "border-style: solid;\n"
            "border-color: black;\n"
            "border-width: 2px;\n"
            "}\n"
            "QPushButton{font: 12pt \"MS Shell Dlg 2\";}\n"
            "\n"
            "QPushButton{\n"
            "background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(243, 243, 243, 255), stop:1 rgba(153, 153, 153, 255));\n"
            "border-style: solid;\n"
            "border-color: black;\n"
            "border-width: 1px;\n"
            "border-radius: 5px;\n"
            "padding : 8px;\n"
            "}\n"
            "QPushButton:pressed{\n"
            "background-color:rgb(157, 195, 255);\n"
            "border-style: solid;\n"
            "border-color: black;\n"
            "border-width: 1px;\n"
            "border-radius: 5px;\n"
            "padding : 8px;\n"
            "}\n"
            "\n"
            "QPushButton:hover{\n"
            "border-color: rgb(0, 85, 127);\n"
            "border-width: 2px;\n"
            "}\n"
            "\n"
            "")
        self.base.setObjectName("base")
        self.gridLayout_12 = QtWidgets.QGridLayout(self.base)
        self.gridLayout_12.setObjectName("gridLayout_12")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setContentsMargins(-1, 50, -1, -1)
        self.verticalLayout_2.setObjectName("verticalLayout_2")

        # Bouton Retour Menu
        self.pushButton_back = QtWidgets.QPushButton(self.base)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_back.sizePolicy().hasHeightForWidth())
        self.pushButton_back.setSizePolicy(sizePolicy)
        self.pushButton_back.setObjectName("pushButton_back")
        self.verticalLayout_2.addWidget(self.pushButton_back, 0, QtCore.Qt.AlignHCenter)

        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.gridLayout_2.addLayout(self.verticalLayout_2, 2, 0, 1, 1)
        self.gridLayout_4 = QtWidgets.QGridLayout()
        self.gridLayout_4.setContentsMargins(50, 50, 50, -1)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.addWidget = QtWidgets.QWidget(self.base)
        self.addWidget.setMaximumSize(QtCore.QSize(300, 500))
        self.addWidget.setStyleSheet("QWidget#addWidget{\n"
                                     "background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(254, 254, 254, 255), stop:1 rgba(216, 216, 216, 255));\n"
                                     "border-radius : 15px;\n"
                                     "}")
        self.addWidget.setObjectName("addWidget")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.addWidget)
        self.verticalLayout_5.setContentsMargins(20, 10, 20, 10)
        self.verticalLayout_5.setObjectName("verticalLayout_5")

        # Label Nom
        self.label_4 = QtWidgets.QLabel(self.addWidget)
        self.label_4.setStyleSheet("font: 12pt \"MS Shell Dlg 2\";")
        self.label_4.setObjectName("label_4")
        self.verticalLayout_5.addWidget(self.label_4)

        # LineEdit Nom
        self.lineEdit_nom = QtWidgets.QLineEdit(self.addWidget)
        self.lineEdit_nom.setStyleSheet("font: 10pt \"MS Shell Dlg 2\";")
        self.lineEdit_nom.setObjectName("lineEdit_nom")
        self.verticalLayout_5.addWidget(self.lineEdit_nom)

        # Bouton Ajouter
        self.pushButton_add = QtWidgets.QPushButton(self.addWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_add.sizePolicy().hasHeightForWidth())
        self.pushButton_add.setSizePolicy(sizePolicy)
        self.pushButton_add.setObjectName("pushButton_add")
        self.verticalLayout_5.addWidget(self.pushButton_add, 0, QtCore.Qt.AlignHCenter)

        self.gridLayout_4.addWidget(self.addWidget, 0, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout_4, 0, 0, 1, 1)
        self.gridLayout_12.addLayout(self.gridLayout_2, 1, 1, 1, 1)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setContentsMargins(50, 20, 50, -1)
        self.verticalLayout.setObjectName("verticalLayout")

        # Label Sélectionner les points de la catégorie
        self.label = QtWidgets.QLabel(self.base)
        self.label.setStyleSheet("font: 75 14pt \"MS Shell Dlg 2\";")
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label, 0, QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)

        # Zone pour la gestion du squelette
        self.label_squelette = MyLabel(self.centralwidget)
        self.label_squelette.setEnabled(True)
        self.label_squelette.setObjectName("label")
        self.label_squelette.setMouseTracking(True)
        self.label_squelette.installEventFilter(self.label_squelette)
        self.verticalLayout.addWidget(self.label_squelette, 1, QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)

        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        self.gridLayout_12.addLayout(self.verticalLayout, 1, 0, 1, 1)
        self.tab.addWidget(self.base, 1, 0, 1, 1)
        self.gridLayout.addWidget(self.all, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 904, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        # Traduction
        self.retranslateUi(MainWindow)

        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        # Affectation des méthodes aux boutons
        self.pushButton_back.clicked.connect(lambda: self.back(MainWindow))
        self.pushButton_add.clicked.connect(lambda: self.addCategorie(self.lineEdit_nom.text(),
                                                                      self.label_squelette.setPoints(), MainWindow))

        # Gestion des données de l'application
        self.data = ControlData()

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    def addCategorie(self, nom, points, MainWindow):
        """ Méthode qui ajoute une catégorie """
        pop = PopUp(MainWindow)
        if pop.champsVide([nom]):
            if pop.selectPoint(points):
                if pop.addElement([nom]):
                    cat = Categorie(nom, points)
                    self.data.addCategorie(cat)
                    # Vide les champs
                    self.lineEdit_nom.setText("")
                    self.label_squelette.fin()


    def back(self, MainWindow):
        """ Méthode qui permet de retourner au menu """
        ui = menu.Ui_MainWindow()
        ui.setupUi(MainWindow)


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    def retranslateUi(self, MainWindow):
        """ Méthode de traduction, qui modifie les textes affichés sur la fenêtre """
        _translate = QtCore.QCoreApplication.translate
        self.label_3.setText(_translate("MainWindow", "Ajout d'une catégorie d'exercices"))
        self.pushButton_back.setText(_translate("MainWindow", "Retour Menu"))
        self.label_4.setText(_translate("MainWindow", "Nom"))
        self.pushButton_add.setText(_translate("MainWindow", "Ajouter"))
        self.label.setText(_translate("MainWindow", "Sélectionner les points de la catégorie"))




######################################### MYLABEL #############################################

class MyLabel(QtWidgets.QLabel):
    """
    Une sous classe de QLabel avec des méthode de dessin et la méthode eventFilter
    """
    def __init__(self, parent=None):
        """ Consructeur de la classe """
        super(MyLabel, self).__init__(parent)
        self.squelette = Squelette()
        self.painter = QPainter()
        self.image = QtGui.QPixmap(config.path_Images + 'squelette.png')
        self.setPixmap(self.image)

    def fin(self):
        """ Méthode qui remet à jour l'image """
        list_ID, list_coord = self.squelette.getAllSelectedPoints()
        for point in list_ID:
            self.squelette.setSelectedPoint(point)
        self.setPixmap(self.image)

    def eventFilter(self, object, event):
        """ Méthode eventFilter avec 2 évènements : clique bouton gauche, survol de la souris """
        # Evènement clique gauche
        if event.type() == QtCore.QEvent.MouseButtonPress:
            mouseEvent = QMouseEvent(event)
            if (mouseEvent.button() == Qt.LeftButton):
                print("Mouse pressed")
                # Récupération de la position de la souris
                x = mouseEvent.pos().x()
                y = mouseEvent.pos().y()
                # Ajoute ou supprime un point
                self.clickAction(x, y)
                return True
        # Evènement survol de la souris
        elif event.type() == QtCore.QEvent.MouseMove:
            mouseEvent = QMouseEvent(event)
            if (mouseEvent.button() == Qt.NoButton):
                print("Mouse hover")
                # Récupération de la position de la souris
                x = mouseEvent.pos().x()
                y = mouseEvent.pos().y()
                self.survolAction(x, y)
                return True
        return super(MyLabel, self).eventFilter(object, event)

    def clickAction(self, x, y):
        """ Méthode qui défini si le point s'affiche ou s'il se supprime en fonction de l'état du point """
        onPoint, point, pos = self.squelette.IsOnPoint(x, y)
        # Si la souris est sur un point
        if onPoint:
            print(point)
            # Si le point n'est pas sélectionné
            if self.squelette.getPointIsSelected(point) == False:
                # Sélection du point
                self.squelette.setSelectedPoint(point)
                self.drawPoint(int(pos[0]), int(pos[1]))
                self.setPixmap(self.pixmap())
            # Si le point est sélectionné
            else:
                # Désélection du point
                self.squelette.setSelectedPoint(point)
                self.dellPoint()
                self.setPixmap(self.pixmap())
        # Si la souris n'est pas sur un point
        else:
            print("pas sur un point")

    def survolAction(self, x, y):
        """ Méthode qui défini si le label s'affiche en fonction de la position de la souris """
        # Si on est sur le point
        onPoint, point, pos = self.squelette.IsOnPoint(x, y)
        if onPoint:
            # Dessine un label
            coord = str(point) + ' - ' + self.squelette.getNamePoint(point)
            x, y = self.majCoordLabel(len(coord)*10, int(pos[0]), int(pos[1]))
            self.drawLabel(coord, x, y)
        # Si on n'est pas ou plus sur le point
        if onPoint == False:
            # Supprime le label
            self.dellPoint()

    def drawPoint(self, x, y):
        """ Méthode qui dessine un point sur le l'image du squelette """
        self.painter.begin(self.pixmap())
        pen = QPen(Qt.darkRed, 2, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin)
        self.painter.setPen(pen)
        self.painter.setBrush(Qt.red)
        self.painter.drawEllipse(x-10, y-10, 20, 20)
        self.painter.end()

    def dellPoint(self):
        """ Méthode qui supprime les éléments dessinés """
        list = self.squelette.getAllSelectedPosition()
        self.setPixmap(self.image)
        for pos in list:
            self.drawPoint(int(pos[0]), int(pos[1]))

    def drawLabel(self, coord, x, y):
        """ Méthode qui dessine un texte sur l'image du squelette """
        self.painter.begin(self.pixmap())

        # Mise en forme pour le rectangle
        pen = QPen(Qt.white, 1, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin)
        self.painter.setBrush(Qt.white)
        self.painter.setOpacity(0.15)
        self.painter.setPen(pen)
        rect = QtCore.QRect(x,y,len(coord)*10,30)
        self.painter.drawRect(rect)

        # Mise en forme pour le texte
        font = QtGui.QFont()
        font.setPixelSize(16)
        self.painter.setFont(font)
        pen = QPen(Qt.black, 1, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin)
        self.painter.setPen(pen)
        self.painter.setOpacity(1.0)
        self.painter.drawText(rect, 0, coord)

        self.painter.end()
        self.setPixmap(self.pixmap())

    def majCoordLabel(self, len_str, x, y):
        """ Méthode qui modifie l'affichage du texte lors du survol en fonction de la position du point """
        if x > self.image.width() - len_str:
            i = x - len_str
        else:
            i = x-20
        j = y-20
        return i, j


    def setPoints(self):
        """ Méthode qui renvoie un chaîne de caractères qui correspond aux points sélectionnés """
        list_ID, list_coord = self.squelette.getAllSelectedPoints()
        points = ''
        for point in list_coord:
            if point == list_coord[-1]:
                points += point
            else:
                points += point + ','
        return points

