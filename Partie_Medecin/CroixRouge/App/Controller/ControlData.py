from Model.BDD import BDD

class ControlData:
    """
    Classe qui permet de gérer les données de l'application
    """
    def __init__(self):
        """
        Constructeur de la classe
        """
        self.bdd = BDD()
        self.listPatient = self.bdd.selectPatient()
        self.listExercice = self.bdd.selectExercice()
        self.listCategorie = self.bdd.selectCategorie()
        self.listProgram = self.bdd.selectAffectationByPatient()

    def formatStr(self, chaine):
        """
        Méthode qui permet de mettre au bon format une chaîne de caractères pour l'insérer dans la base de données
        """
        if "'" in chaine :
            sous_chaine = chaine.split("'")
            res = ""
            for c in sous_chaine :
                if c == sous_chaine[-1] :
                    res += c
                else :
                    res += c +"\\'"
        else :
            res = chaine
        return res

    ######################################### PATIENT #############################################

    def addPatient(self, patient):
        """
        Méthode qui permet d'ajouter un Patient
        """
        patient.nom = self.formatStr(patient.nom)
        patient.prenom = self.formatStr(patient.prenom)
        patient.mdp = self.formatStr(patient.mdp)
        self.bdd.insertPatient(patient)
        self.listPatient = self.bdd.selectPatient()

    def getPatientByID(self, id):
        """
        Méthode qui retourne un Patient en fonction de son id
        """
        pat = self.bdd.selectPatientByID(id)
        return pat[0]


    ######################################### EXERCICE #############################################

    def addExercice(self, exercice):
        """
        Méthode qui permet d'ajouter un Exercice
        """
        exercice.nom = self.formatStr(exercice.nom)
        exercice.video = self.formatStr(exercice.video)
        exercice.fichierJSON = self.formatStr(exercice.fichierJSON)
        exercice.description = self.formatStr(exercice.description)
        exercice.categorie.nom = self.formatStr(exercice.categorie.nom)
        self.bdd.insertExercice(exercice)
        self.listExercice = self.bdd.selectExercice()

    def getExerciceByIDE(self, id):
        """
        Méthode qui retourne un Exercice en fonction de son id
        """
        exo = None
        for ex in self.listExercice:
            if ex.IDE == id:
                exo = ex
                break
        return exo

    def getExerciceByCategorie(self):
        """
        Méthode qui retourne la liste des exercices pour chaque catégorie
        """
        dict_exercices = {}
        i = 0
        list_exercice = []
        # Pour chaque Exercice dans self.listExercice
        for ex in self.listExercice:
            # Si la catégorie de l'Exercice = self.listCategorie[i]
            if ex.categorie.nom == self.listCategorie[i].nom:
                # On ajoute l'Exercice dans la liste
                list_exercice.append(ex)
            # Sinon, on ajoute dans le dicionnaire la liste à la Categorie associée
            else :
                dict_exercices[self.listCategorie[i].nom] = list_exercice
                # On vide la liste des exercices et on ajoute l'Exercice actuel
                list_exercice = []
                list_exercice.append(ex)
                # On change de Categorie dans self.listCategorie
                i += 1
            # Si l'Exercice est le dernier de la liste self.listExercice
            if ex == self.listExercice[-1]:
                # On ajoute dans le dicionnaire la liste à la Categorie associée
                dict_exercices[self.listCategorie[i].nom] = list_exercice
        # Pour chaque Categorie, on vérifie qu'elle est présente dans le dictionnaire, sinon on l'ajoute
        for cat in self.listCategorie:
            if dict_exercices.get(cat.nom) == None:
                dict_exercices[cat.nom] = []
        return dict_exercices


    ######################################### CATEGORIE #############################################

    def addCategorie(self, categorie):
        """
        Méthode qui permet d'ajouter une catégorie
        """
        categorie.nom = self.formatStr(categorie.nom)
        self.bdd.insertCategorie(categorie)
        self.listCategorie = self.bdd.selectCategorie()

    def getCategorieByNom(self, nom):
        """
        Méthode qui retourne une catégorie grâce à son nom
        """
        for cat in self.listCategorie:
            if cat.nom == nom:
                return cat
        return None


    ######################################### PROGRAM #############################################

    def addProgram(self, patient, exercice, nbRepetition, vitesse, tempsPause):
        """
        Méthode qui permet d'ajouter un programme
        """
        self.bdd.insertAffectation(patient, exercice, nbRepetition, vitesse, tempsPause)
        self.listProgram = self.bdd.selectAffectationByPatient()

    def dellProgram(self, patient, exercice):
        """
        Méthode qui permet de supprimer un programme
        """
        self.bdd.deleteAffectation(patient, exercice)
        self.listProgram = self.bdd.selectAffectationByPatient()

    def getProgramByPatient(self, pat):
        """
        Méthode qui retourne le programme d'un Patient donné
        """
        i = 0
        prog = []
        # Tant que le Patient dans self.listProgram[i] != pat
        while i < len(self.listProgram):
            if self.listProgram[i].patient.IDP == pat.IDP:
                prog = self.listProgram[i]
                break
            i += 1
        return prog