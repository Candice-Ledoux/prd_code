######################################### PATIENT #############################################

class Patient():
    """
    Classe qui représente un patient
    """
    def __init__(self, IDP, nom, prenom, mdp):
        """
        Constructeur de la classe
        """
        self.IDP = IDP
        self.nom = nom
        self.prenom = prenom
        self.mdp = mdp


######################################### EXERCICE #############################################

class Exercice():
    """
    Classe qui représente un exercice
    """
    def __init__(self, IDE, nom, fichierJSON, video, description, categorie):
        """
        Constructeur de la classe
        """
        self.IDE = IDE
        self.nom = nom
        self.fichierJSON = fichierJSON
        self.video = video
        self.description = description
        self.categorie = categorie


######################################### CATEGORIE #############################################

class Categorie():
    """
    Classe qui représente une catégorie
    """
    def __init__(self, nom, points):
        """
        Constructeur de la classe
        """
        self.nom = nom
        self.points = points


######################################### PROGRAM #############################################

class Program():
    """
    Classe qui représente un programme qu'un patient doit suivre
    """
    def __init__(self, patient, list_exercice):
        """
        Constructeur de la classe
        """
        self.patient = patient
        self.list_exercice = list_exercice

    def addExercice(self, exercice, nbRepetition, vitesse, tempsPause):
        """
        Méthode permettant d'ajouter un exercice dans le programme du patient
        """
        self.list_exercice.append((exercice, nbRepetition, vitesse, tempsPause))


######################################### PROGRAM REALISE #############################################

class ProgramRealise():
    """
    Classe qui représente le suivi du programme d'un patient
    """

    def __init__(self, patient, list_exercice):
        """
        Constructeur de la classe
        """
        self.patient = patient
        self.list_exercice = list_exercice

    def addExercice(self, exercice, IDR, date, fichierJson):
        """
        Méthode permettant d'ajouter un exercice dans le programme du patient
        """
        self.list_exercice.append((exercice, IDR, date, fichierJson))
