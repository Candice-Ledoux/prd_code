import mysql.connector
from mysql.connector import Error
from Model.Model import *

class BDD():
    """
    Classe qui permet d'effectuer des requêtes sur la base de données
    """
    ####################### CONNEXION ##############################
    def Connexion(self):
        """
        Méthode permettant de se connecter à la base de données
        """
        try:
            connexion = mysql.connector.connect(
                host="localhost",
                port=3306,
                database="croix_rouge",
                user="root",
                password=""
            )
            curseur = connexion.cursor()
            print("Connexion ouverte")

        except Error as e:
            print(e)

        return connexion, curseur


    def FinConnexion(self, connexion, curseur):
        """
        Méthode permettant de fermer la connexion à la base de données
        """
        if (connexion.is_connected()):
            connexion.close()
            curseur.close()
            print("Connexion fermée")


    ####################### PATIENT ##############################

    def selectPatientByNom(self, nom, prenom, mdp):
        """
        Méthode permettant de sélectionner un patient par son nom, son prénom et son mot de passe dans la base
        Retourne un objet patient
        """
        try:
            pat = []
            connexion, curseur = self.Connexion()
            request = "Select * from patient where (Nom = '" + nom + "' AND Prenom = '" + prenom + \
                      "' AND Mot_de_passe = '" + mdp + "'); "
            curseur.execute(request)
            patients = curseur.fetchall()
            for p in patients:
                pat.append(Patient(p[0], p[1], p[2], p[3]))

        except Error as e:
            print(e)

        finally:
            self.FinConnexion(connexion, curseur)
            return pat


    ####################### PROGRAM ##############################

    def selectAffectationByPatient(self, patient):
        """
        Méthode permettant de sélectionner toutes les affectations d'exercices de la base pour un patient donné
        Retourne un objet program
        """
        try:
            prog = Program(patient, [])
            connexion, curseur = self.Connexion()
            request = "SELECT e.*, c.Point, a.NbRepetition, a.Vitesse, a.TempsPause FROM patient p, exercice e, " \
                      "categorie c, affectation a WHERE(a.IDP = " + str(patient.IDP) + " AND e.IDE = a.IDE AND " \
                      "e.NomCategorie = c.NomCategorie) GROUP BY e.IDE"
            curseur.execute(request)
            affectations = curseur.fetchall()

            # Pour chaque ligne dans affectations
            for a in affectations:
                # On récupère les valeurs pour créer une categorie et un exercice
                cat = Categorie(a[5], a[6])
                ex = Exercice(a[0], a[1], a[2], a[3], a[4], cat)
                # On ajoute l'exercice au programme du nouveau patient
                prog.addExercice(ex, a[7], a[8], a[9])

        except Error as e:
            print(e)

        finally:
            self.FinConnexion(connexion, curseur)
            return prog


    ####################### PROGRAM REALISE ##############################

    def insertRealisation(self, patient, exercice, date, fichierJson):
        """
        Méthode permettant d'insérer une réalisation dans la base de données
        """
        try:
            connexion, curseur = self.Connexion()
            request1 = "INSERT INTO realisation (IDR, Date, FichierJsonP, IDP, IDE) VALUES ( 0, '" + str(date) \
                      + "','" + str(fichierJson) + "','" + str(patient.IDP) + "','" + str(exercice.IDE) + "'); "
            curseur.execute(request1)
            connexion.commit()

        except Error as e:
            print(e)

        finally:
            self.FinConnexion(connexion, curseur)