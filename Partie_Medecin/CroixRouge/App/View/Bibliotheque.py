from datetime import time

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

import cv2
import numpy as np

import mediapipe as mp # Import mediapipe
mp_drawing = mp.solutions.drawing_utils # Drawing helpers
mp_pose = mp.solutions.pose # Mediapipe Solutions

import Ressources.config as config
from Controller.ControlData import ControlData
from Controller.Parser import JsonParser
from View.PopUp import PopUp
import View.Menu as menu

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class Ui_MainWindow(QMainWindow):
    """
    Classe IHM de la bibliothèque d'exercice
    """

    def __init__(self, *args, **kwargs):
        """ Constructeur de la classe """
        super(Ui_MainWindow, self).__init__()
        if args[0]:
            self.showMaximized()
        else:
            self.resize(args[1], args[2])
            self.move(args[3], args[4])
        self.setupUi()
        self.show()
   
    def setupUi(self):
        """
        Méthode qui initialise les composants de la fenêtre
        """
        self.setObjectName("MainWindow")
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.all = QtWidgets.QWidget(self.centralwidget)
        self.all.setStyleSheet("QWidget#all{border-radius: 20px;\n"
"blur-raduis:5px;\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 2px;\n"
"pading:50px;\n"
"}")
        self.all.setObjectName("all")
        self.tab = QtWidgets.QGridLayout(self.all)
        self.tab.setContentsMargins(0, 0, 0, 20)
        self.tab.setSpacing(0)
        self.tab.setObjectName("tab")
        self.head = QtWidgets.QWidget(self.all)
        self.head.setStyleSheet("/*QWidget#head{\n"
"    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(255, 255, 255, 255), stop:1 rgba(192, 192, 192, 255));\n"
"    border-style: solid;\n"
"border-color: black;\n"
"border-width: 2px;\n"
"border-radius: 20px;\n"
"}*/")
        self.head.setObjectName("head")
        self.gridLayout_10 = QtWidgets.QGridLayout(self.head)
        self.gridLayout_10.setObjectName("gridLayout_10")

        # Label titre de la fenêtre : Bibliothèque
        self.label_3 = QtWidgets.QLabel(self.head)
        self.label_3.setStyleSheet("font: 75 16pt \"MS Shell Dlg 2\";")
        self.label_3.setObjectName("label_3")

        self.gridLayout_10.addWidget(self.label_3, 0, 0, 1, 1, QtCore.Qt.AlignHCenter)
        self.tab.addWidget(self.head, 0, 0, 1, 1)
        self.base = QtWidgets.QWidget(self.all)
        self.base.setStyleSheet("QWidget#base{background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(108, 219, 215, 255), stop:1 rgba(78, 144, 185, 255));\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 2px;\n"
"}\n"
"QPushButton{font: 12pt \"MS Shell Dlg 2\";}\n"
"\n"
"QPushButton{\n"
"background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(243, 243, 243, 255), stop:1 rgba(153, 153, 153, 255));\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 5px;\n"
"padding : 8px;\n"
"}\n"
"QPushButton:pressed{\n"
"background-color:rgb(157, 195, 255);\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 5px;\n"
"padding : 8px;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"border-color: rgb(0, 85, 127);\n"
"border-width: 2px;\n"
"}\n"
"\n"
"")

        self.base.setObjectName("base")
        self.gridLayout_12 = QtWidgets.QGridLayout(self.base)
        self.gridLayout_12.setObjectName("gridLayout_12")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setContentsMargins(50, 20, 50, -1)
        self.verticalLayout.setObjectName("verticalLayout")

        # Label Selectionner un exercice
        self.label = QtWidgets.QLabel(self.base)
        self.label.setStyleSheet("font: 90 15pt \"MS Shell Dlg 2\";")
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label, 0, QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)

        # ComboBox Categorie
        self.comboBox_membre = QtWidgets.QComboBox(self.base)
        self.comboBox_membre.setStyleSheet("font: 12pt \"MS Shell Dlg 2\";")
        self.comboBox_membre.setObjectName("comboBox_membre")
        self.comboBox_membre.setMinimumSize(QtCore.QSize(200, 30))
        self.comboBox_membre.setMaximumSize(QtCore.QSize(500, 40))
        self.verticalLayout.addWidget(self.comboBox_membre,  0, QtCore.Qt.AlignHCenter)

        # Tableau Exercice
        self.tableWidget_exo = QtWidgets.QTableWidget(self.base)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tableWidget_exo.sizePolicy().hasHeightForWidth())
        self.tableWidget_exo.setSizePolicy(sizePolicy)
        self.tableWidget_exo.setMinimumSize(QtCore.QSize(700, 0))
        self.tableWidget_exo.setStyleSheet("font: 75 12pt \"MS Shell Dlg 2\";")
        self.tableWidget_exo.setObjectName("tableWidget_exo")
        self.tableWidget_exo.setColumnCount(3)
        self.tableWidget_exo.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        item.setSizeHint(QtCore.QSize(100, 40))
        item2 = QtWidgets.QTableWidgetItem()
        item2.setSizeHint(QtCore.QSize(100, 40))
        item3 = QtWidgets.QTableWidgetItem()
        item3.setSizeHint(QtCore.QSize(500, 40))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        item.setFont(font)
        item2.setFont(font)
        item3.setFont(font)
        self.tableWidget_exo.setHorizontalHeaderItem(0, item)
        self.tableWidget_exo.setHorizontalHeaderItem(1, item2)
        self.tableWidget_exo.setHorizontalHeaderItem(2, item3)
        self.tableWidget_exo.horizontalHeader().setStretchLastSection(True)
        self.tableWidget_exo.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.verticalLayout.addWidget(self.tableWidget_exo)

        self.gridLayout_12.addLayout(self.verticalLayout, 1, 0, 1, 1)
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout_4 = QtWidgets.QGridLayout()
        self.gridLayout_4.setContentsMargins(0, 0, 0, -1)
        self.gridLayout_4.setObjectName("gridLayout_4")

        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setContentsMargins(50, 50, 70, -1)
        self.verticalLayout_3.setObjectName("verticalLayout_3")

        # Label Séquence Vidéo
        self.label_2 = QtWidgets.QLabel(self.base)
        self.label_2.setStyleSheet("font: 14pt \"MS Shell Dlg 2\";")
        self.label_2.setObjectName("label_2")
        self.verticalLayout_3.addWidget(self.label_2, 0, QtCore.Qt.AlignHCenter|QtCore.Qt.AlignBottom)

        # Caméra
        self.fps = 24
        self.isCapturing = False
        self.isCapturing = False
        self.gridWidget_camera = QtWidgets.QWidget(self.base)
        sizePolicy.setHeightForWidth(self.gridWidget_camera.sizePolicy().hasHeightForWidth())
        self.cameraWidget = QtWidgets.QLabel(self.base)
        self.cameraWidget.setAlignment(Qt.AlignCenter)
        self.cameraWidget.setEnabled(True)
        self.cameraWidget.setMinimumSize(QtCore.QSize(500, 500))
        self.cameraWidget.setObjectName("cameraWidget")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.cameraWidget)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.verticalLayout_3.addWidget(self.cameraWidget)

        # Boutons
        self.pushButton_replay = QtWidgets.QPushButton(self.base)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_replay.sizePolicy().hasHeightForWidth())
        self.pushButton_replay.setSizePolicy(sizePolicy)
        self.pushButton_replay.setObjectName("pushButton_replay")
        self.verticalLayout_3.addWidget(self.pushButton_replay, 0, QtCore.Qt.AlignHCenter)
        self.pushButton_back = QtWidgets.QPushButton(self.base)
        self.pushButton_back.setObjectName("pushButton_back")
        self.verticalLayout_3.addWidget(self.pushButton_back, 0, QtCore.Qt.AlignHCenter)

        self.gridLayout_4.addLayout(self.verticalLayout_3, 0, 0, 1, 1)

        self.verticalLayout_5 = QtWidgets.QVBoxLayout()
        self.verticalLayout_5.setContentsMargins(0, 50, 50, -1)
        self.verticalLayout_5.setObjectName("verticalLayout_5")

        self.gridLayout_4.addLayout(self.verticalLayout_5, 0, 1, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout_4, 1, 0, 2, 1)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem, 6, 0, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem1, 3, 0, 1, 1)

        self.gridLayout_12.addLayout(self.gridLayout_2, 1, 1, 1, 1)
        self.tab.addWidget(self.base, 1, 0, 1, 1)
        self.gridLayout.addWidget(self.all, 0, 0, 1, 1)
        self.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(self)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 746, 21))
        self.menubar.setObjectName("menubar")
        self.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(self)
        self.statusbar.setObjectName("statusbar")
        self.setStatusBar(self.statusbar)

        # Traduction
        self.retranslateUi()

        QtCore.QMetaObject.connectSlotsByName(self)

        # Initialisation du ComboBox, du tableau et des données de l'application
        self.data = ControlData()
        self.displayCategorie()
        self.ComboboxSelect()

        # Affectation méthodes aux boutons
        self.pushButton_back.clicked.connect(lambda : self.back())
        self.pushButton_replay.clicked.connect(lambda : self.replay())
        self.comboBox_membre.activated.connect(self.ComboboxSelect)

        self.mutex = QMutex()

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    def replay(self):
        """ Méthode qui récupère l'exercice pour le replay de vidéo """
        if self.isCapturing == True:
            self.stop()
        self.exercice = []
        pop = PopUp()
        if pop.selectLine(self.tableWidget_exo.selectedItems()):
            row = self.tableWidget_exo.currentRow()
            Id = self.tableWidget_exo.takeItem(row, 0).text()
            self.ComboboxSelect()
            ex = self.data.getExerciceByIDE(int(Id))
            self.start(ex.fichierJSON, ex.video)

    def setFPS(self, fps):
        """ Méthode qui modifie le fps """
        self.fps = fps

    def start(self, filenameJson, filenameVideo):
        """ Méthode qui charge la vidéo et lance le replay """
        self.cap = cv2.VideoCapture(config.path_Videos + filenameVideo)
        self.isCapturing = True
        print("replay")
        self.timer = QtCore.QTimer()
        self.Affichercamera(filenameJson)
        self.timer.start((1000 / self.fps))


    def stop(self):
        """ Méthode qui stoppe la vidéo """
        if self.isCapturing == True :
            self.isCapturing = False
            self.timer.stop()
            self.cap.release()
            print('fin capture')


    def Affichercamera(self, filename):
        """ Méthode qui affiche les points mediapipe """
        jsonFile = JsonParser()
        dict_mediapipe = dict(jsonFile.readFile(filename))
        idRow = 0
        ret = True

        with mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5) as holistic:
            # Boucle pour parcourir toutes les images de la vidéo
            while (ret):
                # Récupère l'image suivante de la vidéo et un booléen qui indique si l'image existe
                ret, frame = self.cap.read()
                # Vérifie si l'image existe
                if ret == True:
                    # Met l'image en RGB
                    fra = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    # Résultat de mediapipe
                    results = holistic.process(fra)
                    # Modification des landmarks avec ceux contenus dans le fichier JSON
                    row = dict_mediapipe.get(str(idRow))
                    for i in range (0,33):
                        results.pose_landmarks.landmark[i].x = float(row.get('x'+str(i)))
                        results.pose_landmarks.landmark[i].y = float(row.get('y'+str(i)))
                        results.pose_landmarks.landmark[i].z = float(row.get('z'+str(i)))
                        results.pose_landmarks.landmark[i].visibility = 1

                    # Image transformée en fond blanc
                    img = np.zeros([len(fra), len(fra[0]), len(fra[0][0])], dtype=np.uint8)
                    img.fill(255)

                    # Création de QImage
                    pic = QImage(img, img.shape[1], img.shape[0], QImage.Format_RGB888)

                    # Dessine les landmarks sur l'image
                    mp_drawing.draw_landmarks(img, results.pose_landmarks, mp_pose.POSE_CONNECTIONS,
                                    mp_drawing.DrawingSpec(color=(245,117,66), thickness=2, circle_radius=4),
                                    mp_drawing.DrawingSpec(color=(245,66,230), thickness=2, circle_radius=2)
                                    )

                    # Mise à jour du Pixmap et du Label
                    pix = QPixmap.fromImage(pic)
                    self.cameraWidget.setPixmap(pix)
                    idRow += 1

                    # Si la vidéo est interrompue, arrêt de la méthode
                    if cv2.waitKey(100) & 0xFF == ord('q'):
                        break

        # Si la vidéo est encore ouverte, on la ferme
        self.stop()



    def closeEvent(self, event):
        """ Méthode qui ferme l'application et qui permet d'arrêter la caméra si elle fonctionne lors de la fermeture """
        if self.isCapturing == True:
            self.stop()
        event.accept()

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    def ComboboxSelect(self):
        """ Méthode qui affiche les données dans le tableau des exercices """
        self.tableWidget_exo.setRowCount(0)
        self.tableWidget_exo.clearContents()
        dict = self.data.getExerciceByCategorie()
        list_exo = dict.get(self.comboBox_membre.currentText())
        i = 0
        for ex in list_exo:
            self.tableWidget_exo.insertRow(i)
            self.tableWidget_exo.setItem(i, 0, QtWidgets.QTableWidgetItem(str(ex.IDE)))
            self.tableWidget_exo.setItem(i, 1, QtWidgets.QTableWidgetItem(ex.nom))
            self.tableWidget_exo.setItem(i, 2, QtWidgets.QTableWidgetItem(ex.description))
            self.tableWidget_exo.resizeRowToContents(i)
            i = i + 1

    def displayCategorie(self):
        """ Méthode qui affiche les catégories dans le ComboBox """
        for cat in self.data.listCategorie:
            self.comboBox_membre.addItem(cat.nom)


    def back(self):
        """ Méthode qui permet de retourner au menu """
        if self.isCapturing == True:
            self.stop()
        else:
            ui = menu.Ui_MainWindow()
            ui.setupUi(self)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    def retranslateUi(self):
        """ Méthode de traduction, qui modifie les textes affichés sur la fenêtre """
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("MainWindow", "Application Croix Rouge - Médecin"))
        self.label_3.setText(_translate("MainWindow", "Bibliothèque"))
        self.label.setText(_translate("MainWindow", "Selectionner un exercice"))
        self.label_2.setText(_translate("MainWindow", "Séquence vidéo"))                                                                  
        self.pushButton_replay.setText(_translate("MainWindow", "Rejouer"))
        self.pushButton_back.setText(_translate("MainWindow", "Retour Menu"))                                                                     
        item = self.tableWidget_exo.horizontalHeaderItem(0)        
        item.setText(_translate("MainWindow", "ID"))
        item2 = self.tableWidget_exo.horizontalHeaderItem(1)
        item2.setText(_translate("MainWindow", "Nom"))
        item3 = self.tableWidget_exo.horizontalHeaderItem(2)
        item3.setText(_translate("MainWindow", "Description"))
