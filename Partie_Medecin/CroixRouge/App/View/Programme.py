from PyQt5 import QtCore, QtGui, QtWidgets

from Controller.ControlData import ControlData
from View.PopUp import PopUp, PopUpProgram
import View.Patients as patients
from Model.Model import *

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class Ui_MainWindow(object):
        """
        Classe IHM pour la gestion des programmes d'un Patient
        """
        def __init__(self, idp):
                """
                Constructeur de la classe
                """
                self.data = ControlData()
                self.patient = self.data.getPatientByID(int(idp))

        def setupUi(self, MainWindow):
                """
                Méthode qui initialise les composants de la fenêtre
                """
                MainWindow.setObjectName("MainWindow")
                self.centralwidget = QtWidgets.QWidget(MainWindow)
                self.centralwidget.setObjectName("centralwidget")
                self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
                self.gridLayout.setObjectName("gridLayout")
                self.all = QtWidgets.QWidget(self.centralwidget)
                self.all.setStyleSheet("QWidget#all{border-radius: 20px;\n"
        "blur-raduis:5px;\n"
        "border-style: solid;\n"
        "border-color: black;\n"
        "border-width: 2px;\n"
        "pading:50px;\n"
        "}")
                self.all.setObjectName("all")
                self.tab = QtWidgets.QGridLayout(self.all)
                self.tab.setContentsMargins(0, 0, 0, 20)
                self.tab.setSpacing(0)
                self.tab.setObjectName("tab")
                self.head = QtWidgets.QWidget(self.all)
                self.head.setStyleSheet("/*QWidget#head{\n"
        "    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(255, 255, 255, 255), stop:1 rgba(192, 192, 192, 255));\n"
        "    border-style: solid;\n"
        "border-color: black;\n"
        "border-width: 2px;\n"
        "border-radius: 20px;\n"
        "}*/")
                self.head.setObjectName("head")
                self.gridLayout_10 = QtWidgets.QGridLayout(self.head)
                self.gridLayout_10.setObjectName("gridLayout_10")

                # Label Gestion du programme
                self.label_3 = QtWidgets.QLabel(self.head)
                self.label_3.setStyleSheet("font: 75 16pt \"MS Shell Dlg 2\";")
                self.label_3.setObjectName("label_3")
                self.gridLayout_10.addWidget(self.label_3, 0, 0, 1, 1, QtCore.Qt.AlignHCenter)

                self.tab.addWidget(self.head, 0, 0, 1, 1)
                self.base = QtWidgets.QWidget(self.all)
                self.base.setStyleSheet("QLabel{\n"
        "font: 75 12pt \"MS Shell Dlg 2\";\n"
        "}\n"
        "\n"
        "QWidget#base{background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(108, 219, 215, 255), stop:1 rgba(78, 144, 185, 255));\n"
        "border-style: solid;\n"
        "border-color: black;\n"
        "border-width: 2px;\n"
        "}\n"
        "QPushButton{font: 12pt \"MS Shell Dlg 2\";}\n"
        "\n"
        "QPushButton{\n"
        "background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(243, 243, 243, 255), stop:1 rgba(153, 153, 153, 255));\n"
        "border-style: solid;\n"
        "border-color: black;\n"
        "border-width: 1px;\n"
        "border-radius: 5px;\n"
        "padding : 8px;\n"
        "}\n"
        "QPushButton:pressed{\n"
        "background-color:rgb(157, 195, 255);\n"
        "border-style: solid;\n"
        "border-color: black;\n"
        "border-width: 1px;\n"
        "border-radius: 5px;\n"
        "padding : 8px;\n"
        "}\n"
        "\n"
        "QPushButton:hover{\n"
        "border-color: rgb(0, 85, 127);\n"
        "border-width: 2px;\n"
        "}\n"
        "\n"
        "")
                self.base.setObjectName("base")
                self.gridLayout_12 = QtWidgets.QGridLayout(self.base)
                self.gridLayout_12.setObjectName("gridLayout_12")
                self.horizontalLayout = QtWidgets.QHBoxLayout()
                self.horizontalLayout.setObjectName("horizontalLayout")
                spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
                self.horizontalLayout.addItem(spacerItem)

                # Bouton Retour Menu
                self.pushButton_back = QtWidgets.QPushButton(self.base)
                self.pushButton_back.setObjectName("pushButton_back")
                self.horizontalLayout.addWidget(self.pushButton_back)

                spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
                self.horizontalLayout.addItem(spacerItem1)
                self.gridLayout_12.addLayout(self.horizontalLayout, 1, 0, 1, 1)
                self.gridLayout_2 = QtWidgets.QGridLayout()
                self.gridLayout_2.setObjectName("gridLayout_2")
                self.verticalLayout_3 = QtWidgets.QVBoxLayout()
                self.verticalLayout_3.setObjectName("verticalLayout_3")

                # Label Patient
                self.label_patient = QtWidgets.QLabel(self.base)
                self.label_patient.setObjectName("label_patient")
                self.verticalLayout_3.addWidget(self.label_patient, 0, QtCore.Qt.AlignHCenter)

                self.horizontalWidget_2 = QtWidgets.QWidget(self.base)
                self.horizontalWidget_2.setObjectName("horizontalWidget_2")
                self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.horizontalWidget_2)
                self.horizontalLayout_2.setObjectName("horizontalLayout_2")

                # Tableau programme
                self.tableWidget_prog = QtWidgets.QTableWidget(self.base)
                self.tableWidget_prog.setStyleSheet("font: 75 12pt \"MS Shell Dlg 2\";")
                self.tableWidget_prog.setObjectName("tableWidget_prog")
                self.tableWidget_prog.setColumnCount(5)
                self.tableWidget_prog.setRowCount(0)
                item = QtWidgets.QTableWidgetItem()
                item.setSizeHint(QtCore.QSize(0, 40))
                font = QtGui.QFont()
                font.setBold(True)
                item.setFont(font)
                self.tableWidget_prog.setHorizontalHeaderItem(0, item)
                item = QtWidgets.QTableWidgetItem()
                font = QtGui.QFont()
                font.setBold(True)
                item.setFont(font)
                self.tableWidget_prog.setHorizontalHeaderItem(1, item)
                item = QtWidgets.QTableWidgetItem()
                font = QtGui.QFont()
                font.setBold(True)
                item.setFont(font)
                self.tableWidget_prog.setHorizontalHeaderItem(2, item)
                item = QtWidgets.QTableWidgetItem()
                font = QtGui.QFont()
                font.setBold(True)
                item.setFont(font)
                self.tableWidget_prog.setHorizontalHeaderItem(3, item)
                item = QtWidgets.QTableWidgetItem()
                font = QtGui.QFont()
                font.setBold(True)
                item.setFont(font)
                self.tableWidget_prog.setHorizontalHeaderItem(4, item)
                self.tableWidget_prog.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
                self.tableWidget_prog.horizontalHeader().setStretchLastSection(True)
                self.verticalLayout_3.addWidget(self.tableWidget_prog)
                self.gridLayout_2.addLayout(self.verticalLayout_3, 2, 2, 1, 1)

                self.verticalLayout = QtWidgets.QVBoxLayout()
                self.verticalLayout.setObjectName("verticalLayout")
                spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
                self.verticalLayout.addItem(spacerItem2)

                # Boutons ajouter et supprimer
                self.pushButton_add = QtWidgets.QPushButton(self.base)
                self.pushButton_add.setObjectName("pushButton_add")
                self.verticalLayout.addWidget(self.pushButton_add)
                self.pushButton_suppr = QtWidgets.QPushButton(self.base)
                self.pushButton_suppr.setObjectName("pushButton_suppr")
                self.verticalLayout.addWidget(self.pushButton_suppr)

                spacerItem3 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
                self.verticalLayout.addItem(spacerItem3)
                self.gridLayout_2.addLayout(self.verticalLayout, 2, 1, 1, 1)
                self.verticalLayout_2 = QtWidgets.QVBoxLayout()
                self.verticalLayout_2.setObjectName("verticalLayout_2")

                # Label Choisir un Exercice
                self.label = QtWidgets.QLabel(self.base)
                self.label.setObjectName("label")
                self.verticalLayout_2.addWidget(self.label, 0, QtCore.Qt.AlignHCenter)

                # ComboBox Categorie
                self.comboBox_part = QtWidgets.QComboBox(self.base)
                self.comboBox_part.setStyleSheet("font: 75 12pt \"MS Shell Dlg 2\";")
                self.comboBox_part.setObjectName("comboBox_part")
                self.verticalLayout_2.addWidget(self.comboBox_part)

                # Tableau Exercice
                self.tableWidget_lib = QtWidgets.QTableWidget(self.base)
                sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
                sizePolicy.setHorizontalStretch(0)
                sizePolicy.setVerticalStretch(0)
                sizePolicy.setHeightForWidth(self.tableWidget_lib.sizePolicy().hasHeightForWidth())
                self.tableWidget_lib.setSizePolicy(sizePolicy)
                self.tableWidget_lib.setMinimumSize(QtCore.QSize(150, 0))
                self.tableWidget_lib.setStyleSheet("font: 75 12pt \"MS Shell Dlg 2\";")
                self.tableWidget_lib.setObjectName("tableWidget_lib")
                self.tableWidget_lib.setColumnCount(2)
                self.tableWidget_lib.setRowCount(0)
                item = QtWidgets.QTableWidgetItem()
                font = QtGui.QFont()
                font.setBold(True)
                item.setFont(font)
                self.tableWidget_lib.setHorizontalHeaderItem(0, item)
                item = QtWidgets.QTableWidgetItem()
                font = QtGui.QFont()
                font.setBold(True)
                item.setFont(font)
                self.tableWidget_lib.setHorizontalHeaderItem(1, item)
                self.tableWidget_lib.horizontalHeader().setStretchLastSection(True)
                self.tableWidget_lib.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
                self.verticalLayout_2.addWidget(self.tableWidget_lib)
                self.gridLayout_2.addLayout(self.verticalLayout_2, 2, 0, 1, 1)
                self.gridLayout_12.addLayout(self.gridLayout_2, 0, 0, 1, 1)

                self.tab.addWidget(self.base, 1, 0, 1, 1)
                self.gridLayout.addWidget(self.all, 0, 0, 1, 1)
                MainWindow.setCentralWidget(self.centralwidget)
                self.menubar = QtWidgets.QMenuBar(MainWindow)
                self.menubar.setGeometry(QtCore.QRect(0, 0, 720, 21))
                self.menubar.setObjectName("menubar")
                MainWindow.setMenuBar(self.menubar)
                self.statusbar = QtWidgets.QStatusBar(MainWindow)
                self.statusbar.setObjectName("statusbar")
                MainWindow.setStatusBar(self.statusbar)

                # Traduction
                self.retranslateUi(MainWindow)

                QtCore.QMetaObject.connectSlotsByName(MainWindow)

                # Ajustement de la taille des colonnes du tableau Exercice, affichage des categories et des exercices
                self.displayCategorie()
                header = self.tableWidget_lib.horizontalHeader()
                header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
                header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
                self.ComboboxSelectProg()

                # Ajustement de la taille des colonnes du tableau programme et affichage des programmes
                header = self.tableWidget_prog.horizontalHeader()
                header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
                header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
                header.setSectionResizeMode(2, QtWidgets.QHeaderView.Stretch)
                header.setSectionResizeMode(3, QtWidgets.QHeaderView.Stretch)
                header.setSectionResizeMode(4, QtWidgets.QHeaderView.Stretch)

                self.syncProgram()

                # Affectation des méthodes aux boutons
                self.pushButton_back.clicked.connect(lambda : self.back(MainWindow))
                self.pushButton_add.clicked.connect(lambda : self.addProg(MainWindow))
                self.pushButton_suppr.clicked.connect(lambda : self.suppProg(MainWindow))
                self.comboBox_part.activated.connect(self.ComboboxSelectProg)

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        def ComboboxSelectProg(self):
                """ Méthode qui remplit le tableau Exercice """
                self.tableWidget_lib.setRowCount(0)
                self.tableWidget_lib.clearContents()
                dict = self.data.getExerciceByCategorie()
                list_exo = dict.get(self.comboBox_part.currentText())
                i = 0
                for ex in list_exo:
                        self.tableWidget_lib.insertRow(i)
                        self.tableWidget_lib.setItem(i, 0, QtWidgets.QTableWidgetItem(str(ex.IDE)))
                        self.tableWidget_lib.setItem(i, 1, QtWidgets.QTableWidgetItem(str(ex.nom)))
                        self.tableWidget_lib.resizeRowToContents(i)
                        i = i + 1

        def displayCategorie(self):
             """ Méthode qui affiche les catégories dans le ComboBox """
             for cat in self.data.listCategorie:
                self.comboBox_part.addItem(cat.nom)
                
        def syncProgram(self):
                """ Méthode qui remplit le tableau programme """
                self.tableWidget_prog.setRowCount(0)
                self.tableWidget_prog.clearContents()
                prog = self.data.getProgramByPatient(self.patient)
                if prog != [] :
                        i = 0
                        for ex in prog.list_exercice:
                                self.tableWidget_prog.insertRow(i)
                                self.tableWidget_prog.setItem(i, 0, QtWidgets.QTableWidgetItem(str(ex[0].IDE)))
                                self.tableWidget_prog.setItem(i, 1, QtWidgets.QTableWidgetItem(str(ex[0].nom)))
                                self.tableWidget_prog.setItem(i, 2, QtWidgets.QTableWidgetItem(str(ex[1])))
                                self.tableWidget_prog.setItem(i, 3, QtWidgets.QTableWidgetItem(str(ex[2])))
                                self.tableWidget_prog.setItem(i, 4, QtWidgets.QTableWidgetItem(str(ex[3])))
                                self.tableWidget_prog.resizeRowToContents(i)
                                i = i + 1

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        def addProg(self, MainWindow):
                """ Méthode qui ajoute un programme """
                pop = PopUp(MainWindow)
                if pop.selectLine(self.tableWidget_lib.selectedItems()):
                        row = self.tableWidget_lib.currentRow()
                        id = self.tableWidget_lib.takeItem(row, 0).text()
                        self.ComboboxSelectProg()
                        # Exécution du popup programme
                        pop_prog = PopUpProgram(self.pushButton_add)
                        if (pop_prog.nbRepetition != "") and (pop_prog.vitesse != "") and (pop_prog.tempsPause != "") :
                                ex = self.data.getExerciceByIDE(int(id))
                                self.data.addProgram(self.patient, ex, pop_prog.nbRepetition, pop_prog.vitesse, pop_prog.tempsPause)
                                self.syncProgram()
        
        def suppProg(self, MainWindow):
                """ Méthode qui supprime un programme """
                pop = PopUp(MainWindow)
                if pop.selectLine(self.tableWidget_prog.selectedItems()):
                        row = self.tableWidget_prog.currentRow()
                        id = self.tableWidget_prog.takeItem(row, 0).text()
                        ex = self.data.getExerciceByIDE(int(id))
                        self.data.dellProgram(self.patient, ex)
                        self.syncProgram()

        def back(self,MainWindow):
                """ Méthode qui permet de retourner à la gestion des patients """
                ui = patients.Ui_MainWindow()
                ui.setupUi(MainWindow)          

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        def retranslateUi(self, MainWindow):
                """ Méthode de traduction, qui modifie les textes affichés sur la fenêtre """
                _translate = QtCore.QCoreApplication.translate

                self.label_3.setText(_translate("MainWindow", "Gestion des programmes thérapeuthiques"))
                self.pushButton_back.setText(_translate("MainWindow", "Retour"))
                self.label_patient.setText(_translate("MainWindow", "Programme du Patient : " + self.patient.nom
                                                     + ' ' + self.patient.prenom))

                item = self.tableWidget_prog.horizontalHeaderItem(0)
                item.setText(_translate("MainWindow", "ID Exercice"))
                item = self.tableWidget_prog.horizontalHeaderItem(1)
                item.setText(_translate("MainWindow", "Exercice"))
                item = self.tableWidget_prog.horizontalHeaderItem(2)
                item.setText(_translate("MainWindow", "Nombre de répétitions"))
                item = self.tableWidget_prog.horizontalHeaderItem(3)
                item.setText(_translate("MainWindow", "Vitesse"))
                item = self.tableWidget_prog.horizontalHeaderItem(4)
                item.setText(_translate("MainWindow", "Temps de pause"))

                self.pushButton_add.setText(_translate("MainWindow", ">>"))
                self.pushButton_suppr.setText(_translate("MainWindow", "<<"))
                self.label.setText(_translate("MainWindow", "Choisir un Exercice"))
                self.comboBox_part.setItemText(0, _translate("MainWindow", "Catégorie d'exercices"))

                item = self.tableWidget_lib.horizontalHeaderItem(0)
                item.setText(_translate("MainWindow", "ID"))
                item = self.tableWidget_lib.horizontalHeaderItem(1)
                item.setText(_translate("MainWindow", "Exercice"))

        
