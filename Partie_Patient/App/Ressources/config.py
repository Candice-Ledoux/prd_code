""" Ce fichier contient les variables globales qui gère les répertoires utilisés dans l'application """

import os

# Chemin du répertoire courant de l'application
path_App = "App/"

# Chemin du répertoire qui contient toutes les images utilisées dans l'application
path_Images = os.path.join("Ressources/Images/")

# Chemin du répertoire qui contient tous les fichiers JSON qui comporte les points mediapipe des exercices réalisées par le médecin
path_Exercices_Medecin = "../BDD/"

# Chemin du répertoire qui contient tous les fichiers JSON qui comporte les points mediapipe des exercices réalisées par le patient
path_Exercices_Patient = "../Exercices_Patient/"