from PyQt5 import QtCore, QtGui, QtWidgets

from Controller.ControlData import ControlData
from View.PopUp import PopUp
from Model.Model import Patient
import View.Menu as menu
import View.Programme as programme

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class Ui_MainWindow(object):
    """
    Classe IHM pour la visualisation des patients et l'ajout d'un Patient
    """
    def setupUi(self, MainWindow):
        """
        Méthode qui initialise les composants de la fenêtre
        """
        MainWindow.setObjectName("MainWindow")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.all = QtWidgets.QWidget(self.centralwidget)
        self.all.setStyleSheet("QWidget#all{border-radius: 20px;\n"
"blur-raduis:5px;\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 2px;\n"
"pading:50px;\n"
"}")
        self.all.setObjectName("all")
        self.tab = QtWidgets.QGridLayout(self.all)
        self.tab.setContentsMargins(0, 0, 0, 20)
        self.tab.setSpacing(0)
        self.tab.setObjectName("tab")
        self.head = QtWidgets.QWidget(self.all)
        self.head.setStyleSheet("/*QWidget#head{\n"
"    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(255, 255, 255, 255), stop:1 rgba(192, 192, 192, 255));\n"
"    border-style: solid;\n"
"border-color: black;\n"
"border-width: 2px;\n"
"border-radius: 20px;\n"
"}*/")
        self.head.setObjectName("head")
        self.gridLayout_10 = QtWidgets.QGridLayout(self.head)
        self.gridLayout_10.setObjectName("gridLayout_10")

        # Label Gestion des programmes thérapeutiques
        self.label_3 = QtWidgets.QLabel(self.head)
        self.label_3.setStyleSheet("font: 75 16pt \"MS Shell Dlg 2\";")
        self.label_3.setObjectName("label_3")
        self.gridLayout_10.addWidget(self.label_3, 0, 0, 1, 1, QtCore.Qt.AlignHCenter)

        self.tab.addWidget(self.head, 0, 0, 1, 1)
        self.base = QtWidgets.QWidget(self.all)
        self.base.setStyleSheet("QWidget#base{background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(108, 219, 215, 255), stop:1 rgba(78, 144, 185, 255));\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 2px;\n"
"}\n"
"QPushButton{font: 12pt \"MS Shell Dlg 2\";}\n"
"\n"
"QPushButton{\n"
"background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(243, 243, 243, 255), stop:1 rgba(153, 153, 153, 255));\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 5px;\n"
"padding : 8px;\n"
"}\n"
"QPushButton:pressed{\n"
"background-color:rgb(157, 195, 255);\n"
"border-style: solid;\n"
"border-color: black;\n"
"border-width: 1px;\n"
"border-radius: 5px;\n"
"padding : 8px;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"border-color: rgb(0, 85, 127);\n"
"border-width: 2px;\n"
"}\n"
"\n"
"")
        self.base.setObjectName("base")
        self.gridLayout_12 = QtWidgets.QGridLayout(self.base)
        self.gridLayout_12.setObjectName("gridLayout_12")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setContentsMargins(-1, 50, -1, -1)
        self.verticalLayout_2.setObjectName("verticalLayout_2")

        # Boutons
        self.pushButton_gestion = QtWidgets.QPushButton(self.base)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_gestion.sizePolicy().hasHeightForWidth())
        self.pushButton_gestion.setSizePolicy(sizePolicy)
        self.pushButton_gestion.setObjectName("pushButton_gestion")
        self.verticalLayout_2.addWidget(self.pushButton_gestion, 0, QtCore.Qt.AlignHCenter)
        self.pushButton_res = QtWidgets.QPushButton(self.base)
        self.pushButton_res.setEnabled(False)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_res.sizePolicy().hasHeightForWidth())
        self.pushButton_res.setSizePolicy(sizePolicy)
        self.pushButton_res.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.pushButton_res.setObjectName("pushButton_res")
        self.verticalLayout_2.addWidget(self.pushButton_res, 0, QtCore.Qt.AlignHCenter)
        self.pushButton_back = QtWidgets.QPushButton(self.base)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_back.sizePolicy().hasHeightForWidth())
        self.pushButton_back.setSizePolicy(sizePolicy)
        self.pushButton_back.setObjectName("pushButton_back")
        self.verticalLayout_2.addWidget(self.pushButton_back, 0, QtCore.Qt.AlignHCenter)

        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.gridLayout_2.addLayout(self.verticalLayout_2, 2, 0, 1, 1)
        self.gridLayout_4 = QtWidgets.QGridLayout()
        self.gridLayout_4.setContentsMargins(50, 50, 50, -1)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.addWidget = QtWidgets.QWidget(self.base)
        self.addWidget.setMaximumSize(QtCore.QSize(300, 500))
        self.addWidget.setStyleSheet("QWidget#addWidget{\n"
"background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(254, 254, 254, 255), stop:1 rgba(216, 216, 216, 255));\n"
"border-radius : 15px;\n"
"}")
        self.addWidget.setObjectName("addWidget")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.addWidget)
        self.verticalLayout_5.setContentsMargins(20, 10, 20, 10)
        self.verticalLayout_5.setObjectName("verticalLayout_5")

        # Label Ajouter un Patient
        self.horrizontalLayout_51 = QtWidgets.QHBoxLayout()
        self.label_2 = QtWidgets.QLabel(self.addWidget)
        self.label_2.setStyleSheet("\n" "font: 14pt \"MS Shell Dlg 2\";")
        self.label_2.setObjectName("label_2")
        self.verticalLayout_5.addWidget(self.label_2, 0, QtCore.Qt.AlignHCenter)

        # Label et LineEdit Nom
        self.label_4 = QtWidgets.QLabel(self.addWidget)
        self.label_4.setStyleSheet("font: 12pt \"MS Shell Dlg 2\";")
        self.label_4.setObjectName("label_4")
        self.horrizontalLayout_51.addWidget(self.label_4)
        self.lineEdit_nom = QtWidgets.QLineEdit(self.addWidget)
        self.lineEdit_nom.setStyleSheet("font: 10pt \"MS Shell Dlg 2\";")
        self.lineEdit_nom.setObjectName("lineEdit_nom")
        self.horrizontalLayout_51.addWidget(self.lineEdit_nom)
        self.verticalLayout_5.addLayout(self.horrizontalLayout_51)

        # Label et LineEdit Prénom
        self.horrizontalLayout_52 = QtWidgets.QHBoxLayout()
        self.label_8 = QtWidgets.QLabel(self.addWidget)
        self.label_8.setStyleSheet("font: 12pt \"MS Shell Dlg 2\";")
        self.label_8.setObjectName("label_8")
        self.horrizontalLayout_52.addWidget(self.label_8)
        self.lineEdit_prenom = QtWidgets.QLineEdit(self.addWidget)
        self.lineEdit_prenom.setStyleSheet("font: 10pt \"MS Shell Dlg 2\";")
        self.lineEdit_prenom.setObjectName("lineEdit_prenom")
        self.horrizontalLayout_52.addWidget(self.lineEdit_prenom)
        self.verticalLayout_5.addLayout(self.horrizontalLayout_52)

        # Label et LineEdit Mot de passe
        self.horrizontalLayout_53 = QtWidgets.QHBoxLayout()
        self.label_9 = QtWidgets.QLabel(self.addWidget)
        self.label_9.setStyleSheet("font: 12pt \"MS Shell Dlg 2\";")
        self.label_9.setObjectName("label_8")
        self.horrizontalLayout_53.addWidget(self.label_9)
        self.lineEdit_mdp = QtWidgets.QLineEdit(self.addWidget)
        self.lineEdit_mdp.setEchoMode(QtWidgets.QLineEdit.Password)
        self.lineEdit_mdp.setStyleSheet("font: 10pt \"MS Shell Dlg 2\";")
        self.lineEdit_mdp.setObjectName("lineEdit_mdp")
        self.horrizontalLayout_53.addWidget(self.lineEdit_mdp)
        self.verticalLayout_5.addLayout(self.horrizontalLayout_53)

        # CheckBox Afficher le mot de passe
        self.horrizontalLayout_54 = QtWidgets.QHBoxLayout()
        self.checkBox = QtWidgets.QCheckBox(self.addWidget)
        self.checkBox.setStyleSheet("font: 10pt \"MS Shell Dlg 2\";")
        self.verticalLayout_5.addWidget(self.checkBox)

        # Bouton Ajouter
        self.pushButton_add = QtWidgets.QPushButton(self.addWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_add.sizePolicy().hasHeightForWidth())
        self.pushButton_add.setSizePolicy(sizePolicy)
        self.pushButton_add.setObjectName("pushButton_add")
        self.verticalLayout_5.addWidget(self.pushButton_add, 0, QtCore.Qt.AlignHCenter)

        self.gridLayout_4.addWidget(self.addWidget, 0, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout_4, 0, 0, 1, 1)
        self.gridLayout_12.addLayout(self.gridLayout_2, 1, 1, 1, 1)

        # Label Choisir un Patient
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setContentsMargins(50, 20, 50, -1)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.base)
        self.label.setStyleSheet("font: 75 14pt \"MS Shell Dlg 2\";")
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label, 0, QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)

        # Tableau des patients
        self.tableWidget = QtWidgets.QTableWidget(self.base)
        self.tableWidget.setMinimumSize(QtCore.QSize(500, 500))
        self.tableWidget.setStyleSheet("font: 75 12pt \"MS Shell Dlg 2\";")
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(3)
        self.tableWidget.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setBold(True)
        item.setFont(font)
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setBold(True)
        item.setFont(font)
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        font = QtGui.QFont()
        font.setBold(True)
        item.setFont(font)
        self.tableWidget.setHorizontalHeaderItem(2, item)
        self.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.tableWidget.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.verticalLayout.addWidget(self.tableWidget)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        self.gridLayout_12.addLayout(self.verticalLayout, 1, 0, 1, 1)
        self.tab.addWidget(self.base, 1, 0, 1, 1)
        self.gridLayout.addWidget(self.all, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 904, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        # Traduction
        self.retranslateUi(MainWindow)

        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        # Affectation des méthodes aux boutons
        self.pushButton_back.clicked.connect(lambda : self.back(MainWindow))
        self.pushButton_gestion.clicked.connect(lambda : self.prog(MainWindow))
        self.pushButton_add.clicked.connect(lambda : self.addPatient(self.lineEdit_nom.text(),self.lineEdit_prenom.text(),
                                                                     self.lineEdit_mdp.text(), MainWindow))
        self.checkBox.stateChanged.connect(self.checkBoxChangedAction)

        # Ajustement des colonnes du tableau
        header = self.tableWidget.horizontalHeader()
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(2, QtWidgets.QHeaderView.Stretch)

        # Gestion des données de l'application et affichage des données dans le tableau
        self.data = ControlData()
        self.syncData()

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    def addPatient(self, nom, prenom, mdp, MainWindow):
        """ Méthode pour ajouter un Patient """
        pop = PopUp(MainWindow)
        if pop.champsVide([nom, prenom]):
            if pop.mdpCorrect(mdp):
                if pop.addElement([nom, prenom]):
                    pat = Patient(0, nom, prenom, mdp)
                    self.data.addPatient(pat)
                    self.syncData()
                    # Vide les champs
                    self.lineEdit_nom.setText("")
                    self.lineEdit_prenom.setText("")
                    self.lineEdit_mdp.setText("")


    def syncData(self):
        """ Méthode qui remplit le tableau des patients """
        self.tableWidget.setRowCount(0) # Vide avant
        for p in self.data.listPatient:
            rowPosition = self.tableWidget.rowCount()
            self.tableWidget.insertRow(rowPosition)
            self.tableWidget.setItem(rowPosition, 0, QtWidgets.QTableWidgetItem(str(p.IDP)))
            self.tableWidget.setItem(rowPosition, 1, QtWidgets.QTableWidgetItem(p.nom))
            self.tableWidget.setItem(rowPosition, 2, QtWidgets.QTableWidgetItem(p.prenom))
    
    def back(self,MainWindow):
        """ Méthode qui permet de retourner au menu """
        ui = menu.Ui_MainWindow()
        ui.setupUi(MainWindow)

    def prog(self,MainWindow):
        """ Méthode qui permet d'accéder à la vue Gestion du programme du Patient """
        pop = PopUp(MainWindow)
        if pop.selectLine(self.tableWidget.selectedItems()):
            row = self.tableWidget.currentRow()
            idp = self.tableWidget.takeItem(row, 0).text()
            self.syncData()
            ui = programme.Ui_MainWindow(idp)
            ui.setupUi(MainWindow)


    def checkBoxChangedAction(self, state):
        """ Méthode qui affiche ou qui cache le mot de passe """
        if (QtCore.Qt.Checked == state):
            self.lineEdit_mdp.setEchoMode(QtWidgets.QLineEdit.Normal)
        else:
            self.lineEdit_mdp.setEchoMode(QtWidgets.QLineEdit.Password)
        
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    def retranslateUi(self, MainWindow):
        """ Méthode de traduction, qui modifie les textes affichés sur la fenêtre """
        _translate = QtCore.QCoreApplication.translate
        self.label_3.setText(_translate("MainWindow", "Gestion des programmes thérapeutiques"))
        self.pushButton_gestion.setText(_translate("MainWindow", "Gestion du programme thérapeutique"))
        self.pushButton_res.setText(_translate("MainWindow", "Résultat du Patient"))
        self.pushButton_back.setText(_translate("MainWindow", "Retour Menu"))
        self.label_2.setText(_translate("MainWindow", "Ajouter un Patient"))
        self.label_4.setText(_translate("MainWindow", "Nom"))
        self.label_8.setText(_translate("MainWindow", "Prénom"))
        self.label_9.setText(_translate("MainWindow", "Mot de passe"))
        self.pushButton_add.setText(_translate("MainWindow", "Ajouter"))
        self.label.setText(_translate("MainWindow", "Choisir un Patient"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Identifiant"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Nom"))
        item = self.tableWidget.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "Prénom"))
        self.checkBox.setText(_translate("MainWindow", "Afficher le mot de passe"))

