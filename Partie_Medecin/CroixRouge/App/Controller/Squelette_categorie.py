import Ressources.GlobalVar as GBV

import mediapipe as mp # Import mediapipe
mp_pose = mp.solutions.pose # Mediapipe Solutions


class Squelette():
    """
    Classe qui définit le squelette à afficher dans la vue Catégorie
    """
    def __init__(self):
        """ Constructeur de la classe """
        self.nb_points = GBV.nb_points
        self.coord = GBV.coord
        self.setUpPoint()
        self.setUpPosition()

    def setUpPoint(self):
        """ Méthode qui initialise le dictionnaire des points """
        listLabel = [19, 20, 12, 11, 21, 22, 18, 17, 15, 16, 14, 13, 24, 23, 26, 25, 28, 27, 29, 30, 32, 31]
        dict_landmark = self.createLandmarks()
        self.listPoint = {}
        for i in listLabel:
            name = dict_landmark.get(i)
            self.listPoint[i] = {'name' : name, 'selected': False,
                                 'coords': 'x'+str(i)+',y'+str(i)+',z'+str(i)+',v'+str(i)}

    def setUpPosition(self):
        """ Méthode qui initialise le dictionnaire de la position des points """
        listLabel = [19, 20, 12, 11, 21, 22, 18, 17, 15, 16, 14, 13, 24, 23, 26, 25, 28, 27, 29, 30, 32, 31]
        count = 0
        self.listPosition = {}
        for i in range(1, self.nb_points):
            self.listPosition[listLabel[count]] = (self.coord[i][0], self.coord[i][1])
            count += 1
        print(self.listPosition)

    def createLandmarks(self):
        """ Méthode qui récupère les noms des points mediapipe et qui retourne un dictionnaire de données """
        with mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5):
            dict_landmark = {}
            for val in mp_pose.PoseLandmark:
                landmark = str(val)
                landmark = landmark.split('.')
                dict_landmark[val.value] = landmark[1]
        return dict_landmark

    def getAllSelectedPosition(self):
        """ Méthode qui retourne les position de tous les points qui sont sélectionnés """
        list = []
        for key in self.listPoint:
            if self.getPointIsSelected(key):
                list.append(self.listPosition.get(key))
        return list

    def getAllSelectedPoints(self):
        """ Méthode qui retourne les labels des coordonnées pour tous les points sélectionnés """
        list_ID = []
        list_coord = []
        for key in self.listPoint:
            if self.getPointIsSelected(key):
                list_ID.append(key)
                list_coord.append(self.listPoint.get(key).get('coords'))
        return list_ID, list_coord

    def getNamePoint(self, key):
        """ Méthode qui retourne les labels des points mediapipe """
        name = self.listPoint.get(key).get('name')
        return name

    def getCoordPoint(self, key):
        """ Méthode qui retourne les labels des coordonnées mediapipe """
        coords = self.listPoint.get(key).get('coords')
        return coords

    def getPointIsSelected(self, key):
        """ Méthode qui retourne un booléen qui indique si le point est sélectionné """
        selected = self.listPoint.get(key).get('selected')
        return selected

    def setSelectedPoint(self, key):
        """ Méthode qui modifie la valeur de la clé 'selected' dans le dictionnaire des points (True ou False) """
        value = self.listPoint.get(key)
        selected = value.get('selected')
        if selected == False:
            self.listPoint[key]['selected'] = True
        elif selected == True:
            self.listPoint[key]['selected'] = False

    def IsOnPoint(self, x, y):
        """
        Méthode qui cherche si un point est dans la liste des position des point
        Retourne True si le point est dans la liste, False sinon
        Retorune l'identifiant et les coordonnées du point
        """
        onPoint = False
        for key in self.listPosition:
            pos = self.listPosition.get(key)
            # point compris dans l'intervale [x-10,x+10][y-10,y+10]
            if (x > pos[0]-10) and (x <= pos[0]+10):
                if (y > pos[1]-10) and (y <= pos[1]+10):
                    onPoint = True
                    return onPoint, key, (pos[0], pos[1])
        return onPoint, -1, (x,y)
