from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import *
from Model.BDD import BDD


class PopUpUI(QDialog):
    """
    Classe qui gère le popup utilisé dans l'ajout d'un proramme
    """
    def __init__(self, parent):
        """ Consructeur de la classe """
        super().__init__(parent)

        # Vertical Layout
        self.verticalLayout = QtWidgets.QVBoxLayout(self)
        self.verticalLayout.setObjectName("verticalLayout")

        # Horrizontal Layout
        self.horrizontalLayout_1 = QtWidgets.QHBoxLayout(self)
        self.horrizontalLayout_1.setObjectName("horrizontalLayout_1")

        self.horrizontalLayout_2 = QtWidgets.QHBoxLayout(self)
        self.horrizontalLayout_2.setObjectName("horrizontalLayout_2")

        self.horrizontalLayout_3 = QtWidgets.QHBoxLayout(self)
        self.horrizontalLayout_3.setObjectName("horrizontalLayout_3")

        # Nb Répétitions
        self.label_nom = QtWidgets.QLabel()
        self.label_nom.setObjectName("label_nom")

        self.lineEdit_nom = QtWidgets.QLineEdit()
        self.lineEdit_nom.setObjectName("lineEdit_nom")

        self.horrizontalLayout_1.addWidget(self.label_nom)
        self.horrizontalLayout_1.addWidget(self.lineEdit_nom)
        self.verticalLayout.addLayout(self.horrizontalLayout_1)

        # Vitesse
        self.label_prenom = QtWidgets.QLabel()
        self.label_prenom.setObjectName("label_prenom")

        self.lineEdit_prenom = QtWidgets.QLineEdit()
        self.lineEdit_prenom.setObjectName("lineEdit_prenom")

        self.horrizontalLayout_2.addWidget(self.label_prenom)
        self.horrizontalLayout_2.addWidget(self.lineEdit_prenom)
        self.verticalLayout.addLayout(self.horrizontalLayout_2)

        # Temps de pause
        self.label_mdp = QtWidgets.QLabel()
        self.label_mdp.setObjectName("label_mdp")

        self.lineEdit_mdp = QtWidgets.QLineEdit()
        self.lineEdit_mdp.setObjectName("lineEdit_mdp")
        self.lineEdit_mdp.setEchoMode(QtWidgets.QLineEdit.Password)

        self.horrizontalLayout_3.addWidget(self.label_mdp)
        self.horrizontalLayout_3.addWidget(self.lineEdit_mdp)
        self.verticalLayout.addLayout(self.horrizontalLayout_3)

        # CheckBox Afficher le mot de passe
        self.checkBox = QtWidgets.QCheckBox()
        self.checkBox.setObjectName("checkBox")
        self.verticalLayout.addWidget(self.checkBox, 0, QtCore.Qt.AlignVCenter)

        # Boutons
        self.button_Ok = QtWidgets.QPushButton()
        self.button_Ok.setObjectName("button_Ok")
        self.verticalLayout.addWidget(self.button_Ok, 1, QtCore.Qt.AlignHCenter)

        # Action des boutons
        self.checkBox.stateChanged.connect(self.checkBoxChangedAction)
        self.button_Ok.clicked.connect(lambda: self.Click_button_Ok())

        # Affichage des textes des labels
        self.retranslateUi()

        self.setWindowFlags(
            QtCore.Qt.Window |
            QtCore.Qt.CustomizeWindowHint |
            QtCore.Qt.WindowTitleHint |
            QtCore.Qt.WindowCloseButtonHint |
            QtCore.Qt.WindowStaysOnTopHint
        )

        self.exec_()


    def retranslateUi(self):
        """ Méthode de traduction, qui modifie les textes affichés sur la fenêtre """
        _translate = QtCore.QCoreApplication.translate
        self.label_nom.setText(_translate("Popup", "Nom"))
        self.label_prenom.setText(_translate("Popup", "Prénom"))
        self.label_mdp.setText(_translate("Popup", "Mot de passe"))
        self.button_Ok.setText(_translate("Popup", "OK"))
        self.checkBox.setText(_translate("Popup", "Afficher le mot de passe"))

    def checkBoxChangedAction(self, state):
        """ Méthode qui affiche ou qui cache le mot de passe """
        if (QtCore.Qt.Checked == state):
            self.lineEdit_mdp.setEchoMode(QtWidgets.QLineEdit.Normal)
        else:
            self.lineEdit_mdp.setEchoMode(QtWidgets.QLineEdit.Password)

    def Click_button_Ok(self):
        """
        Méthode utilisée lors du clique sur le bouton Ok
        Ferme le popup et met à jour les champs self.nbRepetition, self.vitesse, self.tempsPause
        """
        bdd = BDD()
        self.patient = bdd.selectPatientByNom(self.lineEdit_nom.text(), self.lineEdit_prenom.text(), self.lineEdit_mdp.text())
        self.close()

    def closeEvent(self, QCloseEvent):
        bdd = BDD()
        self.patient = bdd.selectPatientByNom(self.lineEdit_nom.text(), self.lineEdit_prenom.text(),
                                              self.lineEdit_mdp.text())