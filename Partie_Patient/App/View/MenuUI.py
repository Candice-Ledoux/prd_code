# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Menu.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!

from Ressources import config
from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_menuWindow(object):
    """
    Classe IHM du menu
    """
    def setupUi(self, menuWindow):
        """ Méthode qui initialise les composants de la fenêtre """
        menuWindow.setObjectName("menuWindow")
        menuWindow.setAccessibleName("")
        menuWindow.setStyleSheet("background-color: rgb(50, 50, 50);")
        self.centralwidget = QtWidgets.QWidget(menuWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.jsonFileList = QtWidgets.QListView(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(16)
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.jsonFileList.setFont(font)
        self.jsonFileList.viewport().setProperty("cursor", QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.jsonFileList.setStyleSheet("QListView {\n"
                                        "    border: 3px solid #FCFCFC;\n"
                                        "    color: #FCFCFC;\n"
                                        "    text-align: center;\n"
                                        "    show-decoration-selected: 1;\n"
                                        "}\n"
                                        "\n"
                                        "QListView::item:alternate {\n"
                                        "    background: #EEEEEE;\n"
                                        "}\n"
                                        "\n"
                                        "QListView::item:selected:!active {\n"
                                        "    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                        "                                stop: 0 #ABAFE5, stop: 1 #8588B2);\n"
                                        "}\n"
                                        "\n"
                                        "QListView::item:selected:active {\n"
                                        "    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                        "                                stop: 0 #6a6ea9, stop: 1 #888dd9);\n"
                                        "}\n"
                                        "\n"
                                        "QListView::item:selected {\n"
                                        "    border: 1px solid #6a6ea9;\n"
                                        "}\n"
                                        "\n"
                                        "QListView::item:hover {\n"
                                        "    color : black;\n"
                                        "    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
                                        "                                stop: 0 #FAFBFE, stop: 1 #DCDEF1);\n"
                                        "}")
        self.jsonFileList.setLineWidth(1)
        self.jsonFileList.setObjectName("jsonFileList")
        self.gridLayout.addWidget(self.jsonFileList, 0, 0, 1, 1)
        self.splitter = QtWidgets.QSplitter(self.centralwidget)
        self.splitter.setAccessibleName("")
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setOpaqueResize(True)
        self.splitter.setChildrenCollapsible(True)
        self.splitter.setObjectName("splitter")
        self.label = QtWidgets.QLabel(self.splitter)
        self.label.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label.setText("")
        self.label.setPixmap(QtGui.QPixmap(config.path_Images + "select.png"))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.selectSeqButton = QtWidgets.QPushButton(self.splitter)
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.selectSeqButton.setFont(font)
        self.selectSeqButton.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.selectSeqButton.setMouseTracking(False)
        self.selectSeqButton.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.selectSeqButton.setStyleSheet("QPushButton {\n"
                                           "    border: 3px solid #ffffff;\n"
                                           "    color: #ffffff;\n"
                                           "    border-radius: 10px;\n"
                                           "}\n"
                                           "\n"
                                           "QPushButton:hover {\n"
                                           "    background-color: #EAECEE;\n"
                                           "    color: black;\n"
                                           "    border-color: #6a6ea9;\n"
                                           "}\n"
                                           "\n"
                                           "")
        self.selectSeqButton.setInputMethodHints(QtCore.Qt.ImhNone)
        self.selectSeqButton.setCheckable(False)
        self.selectSeqButton.setAutoDefault(False)
        self.selectSeqButton.setDefault(False)
        self.selectSeqButton.setFlat(False)
        self.selectSeqButton.setObjectName("selectSeqButton")
        self.gridLayout.addWidget(self.splitter, 2, 0, 1, 1)
        self.upload_btn = QtWidgets.QPushButton(self.centralwidget)
        self.upload_btn.setMinimumSize(QtCore.QSize(0, 60))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        font.setPointSize(14)
        self.upload_btn.setFont(font)
        self.upload_btn.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.upload_btn.setStyleSheet("QPushButton {\n"
                                      "    border: 3px solid #ffffff;\n"
                                      "    color: #ffffff;\n"
                                      "    border-radius: 10px;\n"
                                      "}\n"
                                      "\n"
                                      "QPushButton:hover {\n"
                                      "    background-color: #EAECEE;\n"
                                      "    color: black;\n"
                                      "    border-color: #6a6ea9;\n"
                                      "}")
        self.upload_btn.setObjectName("upload_btn")
        self.gridLayout.addWidget(self.upload_btn, 4, 0, 1, 1)
        menuWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(menuWindow)
        QtCore.QMetaObject.connectSlotsByName(menuWindow)

    def retranslateUi(self, menuWindow):
        """ Méthode de traduction, qui modifie les textes affichés sur la fenêtre """
        _translate = QtCore.QCoreApplication.translate
        menuWindow.setWindowTitle(_translate("menuWindow", "Turbo Rehab\'"))
        self.selectSeqButton.setText(_translate("menuWindow", "Commencer le programme"))
        self.upload_btn.setText(_translate("menuWindow", "Télécharger un exercice"))


