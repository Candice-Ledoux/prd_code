from PyQt5 import QtCore, QtWidgets
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QMainWindow

from Controller.ControlData import ControlData
from View.PopUp import PopUp
from Model.Model import Exercice
from Controller.Parser import JsonParser
from Controller.Parser import VideoParser
import View.Menu as menu

import cv2
import numpy as np

import mediapipe as mp  # Import mediapipe

mp_drawing = mp.solutions.drawing_utils  # Drawing helpers
mp_pose = mp.solutions.pose  # Mediapipe Solutions


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class Ui_MainWindow(QMainWindow):
    """
    Classe IHM pour l'ajout d'un exercice
    """
    def __init__(self, *args, **kwargs):
        """
        Constructeur de la classe
        """
        super(Ui_MainWindow, self).__init__()
        if args[0]:
            self.showMaximized()
        else:
            self.resize(args[1], args[2])
            self.move(args[3], args[4])
        self.setupUi()
        self.show()

    def setupUi(self):
        """
        Méthode qui initialise les composants de la fenêtre
        """
        self.setObjectName("MainWindow")
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.all = QtWidgets.QWidget(self.centralwidget)
        self.all.setStyleSheet("QWidget#all{border-radius: 20px;\n"
                               "blur-raduis:5px;\n"
                               "border-style: solid;\n"
                               "border-color: black;\n"
                               "border-width: 2px;\n"
                               "pading:50px;\n"
                               "}")
        self.all.setObjectName("all")
        self.tab = QtWidgets.QGridLayout(self.all)
        self.tab.setContentsMargins(0, 0, 0, 20)
        self.tab.setSpacing(0)
        self.tab.setObjectName("tab")
        self.head = QtWidgets.QWidget(self.all)
        self.head.setStyleSheet("/*QWidget#head{\n"
                                "    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(255, 255, 255, 255), stop:1 rgba(192, 192, 192, 255));\n"
                                "    border-style: solid;\n"
                                "border-color: black;\n"
                                "border-width: 2px;\n"
                                "border-radius: 20px;\n"
                                "}*/")
        self.head.setObjectName("head")
        self.gridLayout_10 = QtWidgets.QGridLayout(self.head)
        self.gridLayout_10.setObjectName("gridLayout_10")

        # Label Créer un exercice
        self.label_3 = QtWidgets.QLabel(self.head)
        self.label_3.setStyleSheet("font: 75 16pt \"MS Shell Dlg 2\";")
        self.label_3.setObjectName("label_3")
        self.gridLayout_10.addWidget(self.label_3, 0, 0, 1, 1, QtCore.Qt.AlignHCenter)

        self.tab.addWidget(self.head, 0, 0, 1, 1)
        self.base = QtWidgets.QWidget(self.all)
        self.base.setStyleSheet(
            "QWidget#base{background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(108, 219, 215, 255), stop:1 rgba(78, 144, 185, 255));\n"
            "border-style: solid;\n"
            "border-color: black;\n"
            "border-width: 2px;\n"
            "}\n"
            "")
        self.base.setObjectName("base")
        self.gridLayout_8 = QtWidgets.QGridLayout(self.base)
        self.gridLayout_8.setObjectName("gridLayout_8")
        self.verticalWidget = QtWidgets.QWidget(self.base)
        self.verticalWidget.setMaximumSize(QtCore.QSize(500, 900))
        self.verticalWidget.setStyleSheet("QLabel,QComboBox{\n"
                                          "font: 75 12pt \"MS Shell Dlg 2\";\n"
                                          "}\n"
                                          "\n"
                                          "QPushButton{font: 12pt \"MS Shell Dlg 2\";}\n"
                                          "\n"
                                          "QPushButton{\n"
                                          "background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(243, 243, 243, 255), stop:1 rgba(153, 153, 153, 255));\n"
                                          "border-style: solid;\n"
                                          "border-color: black;\n"
                                          "border-width: 1px;\n"
                                          "border-radius: 5px;\n"
                                          "padding : 8px;\n"
                                          "}\n"
                                          "QPushButton:pressed{\n"
                                          "background-color:rgb(157, 195, 255);\n"
                                          "border-style: solid;\n"
                                          "border-color: black;\n"
                                          "border-width: 1px;\n"
                                          "border-radius: 5px;\n"
                                          "padding : 8px;\n"
                                          "}\n"
                                          "\n"
                                          "QPushButton:hover{\n"
                                          "border-color: rgb(0, 85, 127);\n"
                                          "border-width: 2px;\n"
                                          "}\n"
                                          "")
        self.verticalWidget.setObjectName("verticalWidget")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.verticalWidget)
        self.verticalLayout_3.setObjectName("verticalLayout_3")

        self.horrizontalLayout_31 = QtWidgets.QHBoxLayout()

        # Label ComboBox Categorie
        self.label_Part = QtWidgets.QLabel(self.verticalWidget)
        self.label_Part.setObjectName("label_Part")
        self.horrizontalLayout_31.addWidget(self.label_Part)

        # ComboBox Categorie
        self.comboBox_Part = QtWidgets.QComboBox(self.verticalWidget)
        self.comboBox_Part.setObjectName("comboBox_Part")
        self.comboBox_Part.setMinimumSize(QtCore.QSize(200, 30))
        self.horrizontalLayout_31.addWidget(self.comboBox_Part)
        self.verticalLayout_3.addLayout(self.horrizontalLayout_31)

        # Label et LineEdit nom
        self.horrizontalLayout_32 = QtWidgets.QHBoxLayout()
        self.label_5 = QtWidgets.QLabel(self.verticalWidget)
        self.label_5.setStyleSheet("font: 12pt \"MS Shell Dlg 2\";")
        self.label_5.setObjectName("label_5")
        self.horrizontalLayout_32.addWidget(self.label_5)
        self.textEdit_name = QtWidgets.QLineEdit(self.verticalWidget)
        self.textEdit_name.setMinimumSize(QtCore.QSize(200, 30))
        self.textEdit_name.setStyleSheet("font: 10pt \"MS Shell Dlg 2\";")
        self.textEdit_name.setObjectName("textEdit_name")
        self.horrizontalLayout_32.addWidget(self.textEdit_name)
        self.verticalLayout_3.addLayout(self.horrizontalLayout_32)

        # Label et LineEdit description
        self.horrizontalLayout_33 = QtWidgets.QHBoxLayout()
        self.label_6 = QtWidgets.QLabel(self.verticalWidget)
        self.label_6.setStyleSheet("font: 12pt \"MS Shell Dlg 2\";")
        self.label_6.setObjectName("label_6")
        self.horrizontalLayout_33.addWidget(self.label_6)
        self.textEdit_desc = QtWidgets.QTextEdit(self.verticalWidget)
        self.textEdit_desc.setMinimumSize(QtCore.QSize(200, 300))
        self.textEdit_desc.setStyleSheet("font: 10pt \"MS Shell Dlg 2\";")
        self.textEdit_desc.setObjectName("textEdit_desc")
        self.horrizontalLayout_33.addWidget(self.textEdit_desc)
        self.verticalLayout_3.addLayout(self.horrizontalLayout_33)

        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem)
        self.gridLayout_8.addWidget(self.verticalWidget, 1, 1, 1, 1)

        # Caméra
        self.fps = 24  # Frame per second
        self.isCapturing = False
        self.cameraWidget = QtWidgets.QLabel(self.base)
        self.cameraWidget.setAlignment(Qt.AlignCenter)  # Center the camera stream horizontaly
        self.cameraWidget.setObjectName("cameraWidget")
        self.gridLayout_6 = QtWidgets.QGridLayout(self.cameraWidget)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.gridLayout_8.addWidget(self.cameraWidget, 1, 0, 1, 1)
        # self.start()

        """
        self.cameraWidget.setStyleSheet("border-style: solid;\n"
"border-color: black;\n"
"border-width: 2px;\n"
"border-top-radius: 20px;\n"
"background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(111, 111, 111, 255), stop:0.994318 rgba(156, 156, 156, 255));")
        """

        # Label Prêt à la capture
        self.indicationWidget = QtWidgets.QWidget(self.base)
        self.indicationWidget.setStyleSheet("")
        self.indicationWidget.setObjectName("indicationWidget")
        self.gridLayout_11 = QtWidgets.QGridLayout(self.indicationWidget)
        self.gridLayout_11.setObjectName("gridLayout_11")
        self.label_indication = QtWidgets.QLabel(self.indicationWidget)
        self.label_indication.setStyleSheet("font: 75 25pt \"MS Shell Dlg 2\";")
        self.label_indication.setObjectName("label_indication")
        self.gridLayout_11.addWidget(self.label_indication, 0, 0, 1, 1, QtCore.Qt.AlignHCenter)
        self.gridLayout_8.addWidget(self.indicationWidget, 0, 0, 1, 1)

        self.rejoueWidget = QtWidgets.QWidget(self.base)
        self.rejoueWidget.setStyleSheet("\n"
                                        "QPushButton{font: 12pt \"MS Shell Dlg 2\";}\n"
                                        "\n"
                                        "QPushButton{\n"
                                        "background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(243, 243, 243, 255), stop:1 rgba(153, 153, 153, 255));\n"
                                        "border-style: solid;\n"
                                        "border-color: black;\n"
                                        "border-width: 1px;\n"
                                        "border-radius: 5px;\n"
                                        "padding : 8px;\n"
                                        "}\n"
                                        "QPushButton:pressed{\n"
                                        "background-color:rgb(157, 195, 255);\n"
                                        "border-style: solid;\n"
                                        "border-color: black;\n"
                                        "border-width: 1px;\n"
                                        "border-radius: 5px;\n"
                                        "padding : 8px;\n"
                                        "}\n"
                                        "\n"
                                        "QPushButton:hover{\n"
                                        "border-color: rgb(0, 85, 127);\n"
                                        "border-width: 2px;\n"
                                        "}\n"
                                        "")

        self.rejoueWidget.setObjectName("rejoueWidget")
        self.gridLayout_9 = QtWidgets.QGridLayout(self.rejoueWidget)
        self.gridLayout_9.setObjectName("gridLayout_9")
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_9.addItem(spacerItem1, 0, 4, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_9.addItem(spacerItem2, 0, 2, 1, 1)

        # Boutons
        self.pushButton_replay = QtWidgets.QPushButton(self.rejoueWidget)
        self.pushButton_replay.setObjectName("pushButton_replay")
        self.gridLayout_9.addWidget(self.pushButton_replay, 0, 3, 1, 1)
        self.pushButton_Begin = QtWidgets.QPushButton(self.rejoueWidget)
        self.pushButton_Begin.setObjectName("pushButton_Begin")
        self.gridLayout_9.addWidget(self.pushButton_Begin, 0, 1, 1, 1)
        self.pushButton_add = QtWidgets.QPushButton(self.rejoueWidget)
        self.pushButton_add.setObjectName("pushButton_add")
        self.gridLayout_9.addWidget(self.pushButton_add, 0, 5, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_9.addItem(spacerItem3, 0, 0, 1, 1)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_9.addItem(spacerItem4, 0, 6, 1, 1)
        self.gridLayout_8.addWidget(self.rejoueWidget, 2, 0, 1, 1)
        self.pushButton_back = QtWidgets.QPushButton(self.base)
        self.pushButton_back.setStyleSheet("\n"
                                           "QPushButton{font: 12pt \"MS Shell Dlg 2\";}\n"
                                           "\n"
                                           "QPushButton{\n"
                                           "background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(243, 243, 243, 255), stop:1 rgba(153, 153, 153, 255));\n"
                                           "border-style: solid;\n"
                                           "border-color: black;\n"
                                           "border-width: 1px;\n"
                                           "border-radius: 5px;\n"
                                           "padding : 8px;\n"
                                           "}\n"
                                           "QPushButton:pressed{\n"
                                           "background-color:rgb(157, 195, 255);\n"
                                           "border-style: solid;\n"
                                           "border-color: black;\n"
                                           "border-width: 1px;\n"
                                           "border-radius: 5px;\n"
                                           "padding : 8px;\n"
                                           "}\n"
                                           "\n"
                                           "QPushButton:hover{\n"
                                           "border-color: rgb(0, 85, 127);\n"
                                           "border-width: 2px;\n"
                                           "}\n"
                                           "")
        # Bouton Retour Menu
        self.pushButton_back.setObjectName("pushButton_back")
        self.pushButton_back.setMaximumSize(QtCore.QSize(200, 40))
        self.gridLayout_8.addWidget(self.pushButton_back, 2, 1, 1, 1)

        self.tab.addWidget(self.base, 1, 0, 1, 1)
        self.gridLayout.addWidget(self.all, 0, 0, 1, 1)
        self.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(self)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 893, 21))
        self.menubar.setObjectName("menubar")
        self.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(self)
        self.statusbar.setObjectName("statusbar")
        self.setStatusBar(self.statusbar)

        # Traduction
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self)

        # Objets pour le gestion de la vidéo
        self.dict_mediapipe = {}
        self.list_images = []

        # Affectation des méthodes aux boutons
        self.pushButton_back.clicked.connect(lambda: self.back())
        self.pushButton_Begin.clicked.connect(lambda: self.start())
        self.pushButton_replay.clicked.connect(lambda: self.stop())
        self.pushButton_add.clicked.connect(lambda: self.addExercice(self.textEdit_name.text(),
                                                                     self.textEdit_desc.toPlainText(),
                                                                     self.comboBox_Part.currentText()))

        # Initialisation du ComboBox
        self.displayCategorie()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def displayCategorie(self):
        """ Méhode pour afficher les catégories dans le ComboBox """
        self.data = ControlData()
        for cat in self.data.listCategorie:
            self.comboBox_Part.addItem(cat.nom)

    def back(self):
        """ Méthode qui permet de retourner au menu """
        self.stop()
        ui = menu.Ui_MainWindow()
        ui.setupUi(self)

    def addExercice(self, nom, description, nom_cat):
        """ Méthode qui permet d'ajouter un exercice' """
        pop = PopUp(self)
        if pop.champsVide([nom, nom_cat]):
            if pop.captureIsPresent(self.dict_mediapipe):
                if pop.addElement([nom]):
                    # Crée le fichiers JSON et ajoute les données
                    fichier = JsonParser()
                    filenameJson = fichier.writeFile(nom, self.dict_mediapipe)
                    # Crée la vidéo de l'exercice
                    video = VideoParser()
                    filenameVideo = video.writeFile(nom, self.fps, self.list_images)
                    # Crée la catégorie et l'exercice, puis l'ajoute dans la base
                    cat = self.data.getCategorieByNom(nom_cat)
                    ex = Exercice(0, nom, filenameJson, filenameVideo, description, cat)
                    self.data.addExercice(ex)
                    # Vide les champs
                    self.textEdit_name.setText("")
                    self.textEdit_desc.setText("")
                    self.dict_mediapipe = {}
                    self.list_images = []

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def getLandmarks(self):
        """ Méthode qui récupère les landmarks de Pose (mediapipe) """
        with mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5):
            landmarks = []
            for val in mp_pose.PoseLandmark:
                landmarks += ['x{}'.format(val.value), 'y{}'.format(val.value), 'z{}'.format(val.value),
                              'v{}'.format(val.value)]
        return landmarks

    def start(self):
        """ Méthode qui lance la capture vidéo  """
        self.stop()
        """ Méthode qui lance la caméra """
        self.cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
        self.isCapturing = True
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.StartClicked)
        self.timer.start((1000 / self.fps))

    def stop(self):
        """ Méthode qui stoppe la capture vidéo """
        if self.isCapturing == True:
            self.isCapturing = False
            self.timer.stop()
            self.cap.release()
            print("fin enregistrement")

    def setFPS(self, fps):
        """ Méthode pour modifier le fps """
        self.fps = fps

    def StartClicked(self):
        """ Méthode pour enregistrer une vidéo et extraire les points dans un fichier CSV """
        print("Début enregistrement")
        landmarks = self.getLandmarks()
        row = 0
        with mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5) as holistic:
            # Tant que le caméra est ouverte
            while self.cap.isOpened():
                ret, fra = self.cap.read()
                # Converti l'image BGR vers RGB pour la vidéo
                cv2.flip(fra, +1, fra)
                frame = fra.copy()
                pic = cv2.cvtColor(fra, cv2.COLOR_BGR2RGB)
                self.list_images.append(pic)
                # Converti l'image BGR vers RGB pour le fichier JSON
                imge = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

                # Détections du squelette
                results = holistic.process(imge)
                # QImage
                im = QImage(imge, imge.shape[1], imge.shape[0], QImage.Format_RGB888)
                # Dessine sur l'image
                mp_drawing.draw_landmarks(imge, results.pose_landmarks, mp_pose.POSE_CONNECTIONS,
                                          mp_drawing.DrawingSpec(color=(245, 117, 66), thickness=2, circle_radius=4),
                                          mp_drawing.DrawingSpec(color=(245, 66, 230), thickness=2, circle_radius=2)
                                          )

                # Met à jour le PixMap du Label
                pix = QPixmap.fromImage(im)
                self.cameraWidget.setPixmap(pix)

                # self.list_images.append(imge)

                # Récupère les landmark et les stocke dans un dictionnaire de données
                pose = results.pose_landmarks.landmark
                pose_row = list(np.array([[landmark.x, landmark.y, landmark.z, landmark.visibility]
                                          for landmark in pose]).flatten())

                dict_image = {}
                for i in range(len(landmarks)):
                    dict_image[landmarks[i]] = pose_row[i]
                self.dict_mediapipe[row] = dict_image
                row += 1

                # Si la vidéo est interrompue, on quitte la méthode
                if cv2.waitKey(10) & 0xFF == ord('q'):
                    break

    def closeEvent(self, event):
        """ Méthode qui ferme l'application et qui permet d'arrêter la caméra si elle fonctionne lors de la fermeture """
        self.stop()
        event.accept()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def retranslateUi(self):
        """ Méthode de traduction, qui modifie les textes affichés sur la fenêtre """
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("MainWindow", "Application Croix Rouge - Médecin"))
        self.label_3.setText(_translate("MainWindow", "Créer un exercice"))
        self.label_5.setText(_translate("MainWindow", "Nom de l'exercice"))
        self.label_Part.setText(_translate("MainWindow", "Catégorie"))
        self.label_6.setText(_translate("MainWindow", "Description"))
        self.label_indication.setText(_translate("MainWindow", "Prêt à la capture "))
        self.pushButton_replay.setText(_translate("MainWindow", "Fin Capture"))
        self.pushButton_Begin.setText(_translate("MainWindow", "Commencer l'enregistrement"))
        self.pushButton_add.setText(_translate("MainWindow", "Ajouter l'exercice"))
        self.pushButton_back.setText(_translate("MainWindow", "Retour Menu"))
