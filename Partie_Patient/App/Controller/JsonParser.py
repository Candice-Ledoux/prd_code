import json
import datetime
import os

class JsonParser():
    """
    Classe qui permet de lire et écrire des fichiers JSON
    """
    def writeFile(self, directory, name, repetition, dict_mediapipe):
        """
        Méthode qui permet de créer un fichier JSON et d'écrire dans celui-ci
        Retourne le nom du fichier créé et la date de création
        """
        date = datetime.datetime.now()
        filename = name + '_' + str(date.year) + '-' + str(date.month) + '-' + str(date.day) + '_' + str(date.hour) +\
                   '-' + str(date.minute) + '__R' + str(repetition) + '.json'
        rep_cour = os.getcwd()
        os.chdir(directory)
        with open(filename, 'w') as fichier:
            json.dump(dict_mediapipe, fichier)
        os.chdir(rep_cour)
        return filename, date

    def readFile(self, directory, filename):
        """
        Méthode qui permet de lire un fichier JSON grâce à son nom et au répertoire où il se trouve
        Retourne un dictionnaire qui correspond aux données stockées dans le fichier
        """
        rep_cour = os.getcwd()
        os.chdir(directory)
        with open(filename, 'r') as fichier:
            dict_mediapipe = json.load(fichier)
        os.chdir(rep_cour)
        return dict_mediapipe
