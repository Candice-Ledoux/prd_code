from PyQt5 import QtGui, QtWidgets

import Ressources.config as config
from View.Menu import Ui_MainWindow
from Model.BDD import BDD

import ctypes
import sys
import os


def main():
    """ Méthode pour lancer l'application """
    bdd = BDD()
    bdd.CreerDataBase()

    app = QtWidgets.QApplication(sys.argv)

    app.setWindowIcon(QtGui.QIcon(config.path_Images + 'icon.png'))
    myappid = 'croix.rouge.app.medecin.v2'  # chaîne de caractère aléatoire
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)

    if not os.path.exists(config.path_Fichiers):
        os.makedirs(config.path_Fichiers)
    if not os.path.exists(config.path_Videos):
        os.makedirs(config.path_Videos)

    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    """ Exécution du Main """
    main()
